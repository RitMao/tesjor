@extends('admin.layout.layout')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Register</h3>
                    </div>
                    <div class="panel-body">
                        @if(Session::has('success-message'))
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{Session::get('success-message')}}

                            </div>
                        @endif
                        @if(Session::has('error-message'))
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{Session::get('error-message')}}

                            </div>
                        @endif
                        {!! Form::open(array('url' => route('post-register-user'))) !!}

                            <fieldset>
                                <div class="form-group">
                                    <label>Email</label>
                                    {{Form::text('email',null,array('class' => 'form-control','placeholder' => 'E-mail','autofocus','required')) }}
                                    {{--<input class="form-control" placeholder="E-mail" name="email" type="email" autofocus>--}}
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    {{Form::password('password',array('class' => 'form-control','placeholder' => 'Password','required')) }}

                                </div>
                                <div class="form-group">
                                    <label>First Name</label>
                                       {{Form::text('first_name',null,array('class' => 'form-control'))}}

                                </div>
                                <div class="form-group">
                                    <label>Last Name</label>
                                    {{Form::text('last_name',null,array('class' => 'form-control'))}}

                                </div>
                                <div class="form-group">
                                    <label>Position</label>
                                    {{Form::text('position',null,array('class' => 'form-control'))}}

                                </div>
                                <div class="form-group">
                                    <label>Gender</label>
                                    {{Form::text('gender',null,array('class' => 'form-control'))}}

                                </div>
                                <div class="form-group">
                                    <label>Date of Birth</label>
                                    {{Form::text('date_of_birth',null,array('class' => 'form-control'))}}

                                </div>
                                <div class="form-group">
                                    <label>Phone Number</label>
                                    {{Form::text('phone_number',null,array('class' => 'form-control'))}}

                                </div>
                                <div class="form-group">
                                    <label> Title</label>
                                    {{Form::text('title',null,array('class' => 'form-control'))}}

                                </div>
                                <div class="form-group">
                                    <label>City Code</label>
                                    {{Form::text('city_code',null,array('class' => 'form-control'))}}

                                </div>
                                <div class="form-group">
                                    <label>State Code</label>
                                    {{Form::text('state_code',null,array('class' => 'form-control'))}}

                                </div>
                                <div class="form-group">
                                    <label>Country Code</label>
                                    {{Form::text('country_code',null,array('class' => 'form-control'))}}

                                </div>
                                <div class="form-group">
                                    <label>Street Address</label>
                                    {{Form::text('street_address',null,array('class' => 'form-control'))}}

                                </div>
                                <div class="form-group">
                                    <label>Sector Cd</label>
                                    {{Form::text('sector_cd',null,array('class' => 'form-control'))}}

                                </div>
                                <!-- Change this to a button or input when using this as a form -->

                                {{Form::submit('Submit',array('class' => 'btn btn-lg btn-success btn-block'))}}
                            </fieldset>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
