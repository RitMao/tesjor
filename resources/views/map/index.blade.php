
<html>
<head>
	<title>

	</title>
</head>
<body>
<div id="googleMap" style="width:100%;height:100%;position:absolute"></div>


	<script type="text/javascript">
		function initMap()
		{
			var directionsService = new google.maps.DirectionsService;
			var directionsDisplay = new google.maps.DirectionsRenderer;


			var map = new google.maps.Map(document.getElementById("googleMap"), {
				center: new google.maps.LatLng(11.555606, 104.874228),
				zoom: 12
			});

			directionsDisplay.setMap(map);

			var infoWindow = new google.maps.InfoWindow({map: map});

			//use html5 geolocation
			if(navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(function(position) {
					var current = {
						lat: position.coords.latitude,
						lng: position.coords.longitude
					};

					var to = {
						lat: {{ $data['latitude'] }},
						lng: {{ $data['longitude'] }}
				    }

					directionsService.route({
						origin: current,
						destination: to,
						travelMode: google.maps.TravelMode.DRIVING

					}, function(response, status) {
						if (status === google.maps.DirectionsStatus.OK) {
							directionsDisplay.setDirections(response);
						} else {
							window.alert('Directions request failed due to ' + status);
						}
					});

					map.setCenter(current);

				}, function() {
					handleLocationError(true, infoWindow, map.getCenter());
				});

			} else{
				// Browser doesn't support Geolocation
				handleLocationError(false, infoWindow, map.getCenter());


			}
		}

		function handleLocationError(browserHasGeolocation, infoWindow, pos) {

			infoWindow.setPosition(pos);
			infoWindow.setContent(browserHasGeolocation ?
					'Error: The Geolocation service failed.' :
					'Error: Your browser doesn\'t support geolocation.');
		}
	</script>

	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC8ivLfYZg4MrUH65J6LYr11wjEOywyxDA&callback=initMap"></script>

</body>
</html>


