@extends('admin.layout.layout')
@include('admin.layout.menu')
@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page-header">Add Promotion</h1>
            </div>

            <!-- /.col-md-12 -->
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-12">Add Promotion</div>

                        </div>
                    </div>
                    <!-- /.panel-heading -->

                    <div class="panel-body">
                        {!! Form::open(array('url' => route('post-add-promotion'),'id' => 'frm-restaurant-promotion')) !!}
                        @if(Session::has('success-message'))
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{Session::get('success-message')}}

                            </div>
                        @endif
                        @if(Session::has('error-message'))
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{Session::get('error-message')}}

                            </div>
                        @endif
                        <input type="hidden" name="res_id" value="{{$id}}">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-2">
                                        Promotion Type:
                                    </div>
                                    <div class="col-md-10">
                                        <div class="form-group">
                                      {!! Form::select('promotion_type_cd',$pro_cd,null,array('class' => 'form-control','id' => 'pro_type')) !!}
                                            </div>
                                    </div>
                                </div>
                                <div class="row" >
                                    <div class="col-md-2">
                                        Description:
                                    </div>
                                    <div class="col-md-10">
                                        <div class="form-group">
                                        {{ Form::textarea('description', null, ['cols' => '40','rows' => '9', 'class' => 'form-control']) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin-bottom: 5px;">
                                    <div class="col-md-2">
                                        Policy:
                                    </div>
                                    <div class="col-md-10">
                                        <div class="form-group">
                                        {{ Form::textarea('policy', null, ['cols' => '40','rows' => '6', 'class' => 'form-control']) }}
                                        </div>
                                    </div>
                                </div>

                                <div class="row" >
                                    <div class="col-md-2">
                                        Promotion Date:
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            Start Date
                                        <div class="input-group">

                                            <input id="star-date" name="start_date" type="text" class="date-picker form-control start-date" value="{{ Carbon\Carbon::today()->format('Y-m-d')}}" />
                                            <label class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>

                                            </label>
                                        </div>
                                            </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            End Date
                                            <div class="input-group">

                                                <input id="star-date" name="end_date" type="text" class="date-picker form-control start-date" value="{{ Carbon\Carbon::today()->format('Y-m-d')}}" />
                                                <label class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>

                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">

                                <div class="row">

                                    <div class="col-md-offset-2 col-md-10">
                                        <table id="promotion-table" style="width: 100% !important;">
                                            <tr>
                                                <td>

                                                </td>

                                            </tr>
                                        </table>



                                        <div class="upload-image-promotion" id="banner-promotion" >
                                            <input accept="image/gif, image/jpeg, image/png" type="file" class="v-hidden" id="promo-img" name="promo_img">
                                            <i class="fa fa-plus-circle fa-5x img-add-circle" aria-hidden="true" id="add-promotion" style="cursor: pointer;"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" >
                                    <div class="col-md-2">
                                        Term Condition:
                                    </div>
                                    <div class="col-md-10">
                                        <div class="form-group">
                                            {{ Form::textarea('term', null, ['cols' => '40','rows' => '6', 'class' => 'form-control']) }}
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div id="allocate" style="display: none;">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Credit Vouchers:</label>
                                    <div class="title-underline"></div>
                                </div>

                            </div>
                            <div class="row">
                                    <div class="col-md-1">

                                        Allocate Credit:

                                    </div>
                                    <div class="col-md-2">

                                        <div class="form-group">
                                            {{Form::text('promotion_credit_amt',null,array('class' => 'form-control', 'id' => 'name'))}}
                                            <font size="1px">(Allocated credit amount will be lasped on last date of calendar month)</font>
                                        </div>

                                    </div>
                                <div class="col-md-1">

                                    Periodical Expired:

                                </div>
                                <div class="col-md-2">

                                    <div class="form-group">
                                        {{Form::text('periodical_expire',(empty($period)?'':$period),array('class' => 'form-control', 'id' => 'name'))}}

                                    </div>

                                </div>
                                </div>
                        </div>
                        <div id="credit_voucher" style="display: none;">
                            <div class="row">
                                <div class="col-md-12">

                                    <label>Promotion Conditions:</label>
                                    <div class="title-underline"></div>

                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12 text-right" style="margin-bottom: 5px !important;"><button class="btn btn-primary btn-circle" id="addrow-cupon" type="button"><i class="fa fa-plus" aria-hidden="true"></i></button> </div>
                            </div>
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="table-responsive">

                                        <table class="table table-bordered" id="cupon-table">
                                            <thead>
                                            <tr>
                                                <td>Minimum Order Value</td>
                                                <td>Maximum Order Value</td>
                                                <td>Product</td>
                                                <td>Applicable Days Time</td>

                                            </tr>
                                            </thead>
                                            <tbody id="cupon-body">
                                            <tr>

                                                <td>
                                                    <div class="input-group">
                                                        <input type="text" name="minimum_order_amt_CGP[]" class="form-control" value="<?php echo (empty($mincoupon)?'':$mincoupon)?>">
                                                        <span class="input-group-addon">$</span>
                                                    </div>

                                                </td>
                                                <td>
                                                    <div class="input-group">
                                                        <input type="text" name="maximum_order_amt_CGP[]" class="form-control">
                                                        <span class="input-group-addon">$</span>
                                                    </div>


                                                </td>
                                                <td>

                                                    <select name="promotion_detail_product_CGP_0[]" class="form-control applicable_item" multiple  style="width:300px !important;">
                                                        <option selected>None</option>

                                                    </select>


                                                </td>

                                                <td>
                                                    <div id="add-work-hour">

                                                                <table >
                                                                    <tr>
                                                                        <td>
                                                                            <table id="table-time">
                                                                                <tr>
                                                                                    <td> <div class="input-group bootstrap-timepicker timepicker" style="width: 200px !important;">
                                                                                            <input  type="text" name="start_time_CGP[]" class="form-control input-small timepicker1" >
                                                                                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                                                                        </div></td>
                                                                                    <td> To </td>
                                                                                    <td> <div class="input-group bootstrap-timepicker timepicker"  style="width: 200px !important;">
                                                                                            <input  type="text" name="end_time_CGP[]" class="form-control input-small timepicker2">
                                                                                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                                                                        </div></td>
                                                                                </tr>
                                                                                <tr id="row-time">
                                                                                    <td colspan="3">
                                                                                        <ul class="days-week">
                                                                                            <input type="hidden" name="allcheck_CGP[]" class="allcheck_CGP">
                                                                                            <li><label><input type="checkbox" class="checkmon checkDayCGP" name="check[]" style="display:none;"  value="Mon"><a onclick="dayofmon(this);">Mon</a></label></li>
                                                                                            <li><label><input type="checkbox" class="checktue checkDayCGP" name="check[]" style="display:none;"  value="Tue"><a onclick="dayoftue(this);"> Tue</a></label></li>
                                                                                            <li><label><input type="checkbox" class="checkwed checkDayCGP" name="check[]" style="display:none;"  value="Wed"><a onclick="dayofwed(this);"> Wed</a></label></li>
                                                                                            <li><label><input type="checkbox" class="checkthu checkDayCGP" name="check[]" style="display:none;" value="Thu"><a onclick="dayofthu(this);"> Thu</a></label></li>
                                                                                            <li><label><input type="checkbox" class="checkfri checkDayCGP" name="check[]" style="display:none;" value="Fri"><a onclick="dayoffri(this);"> Fri</a></label></li>
                                                                                            <li><label><input type="checkbox" class="checksat checkDayCGP" name="check[]" style="display:none;" value="Sat"><a onclick="dayofsat(this);"> Sat</a></label></li>
                                                                                            <li><label><input type="checkbox" class="checksun checkDayCGP" name="check[]" style="display:none;" value="Sun"><a onclick="dayofsun(this);"> Sun</a></label></li>


                                                                                        </ul>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>




                                                                        </td>


                                                                    </tr>
                                                                </table>


                                                    </div>
                                                </td>

                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div id="discount" >
                            <div class="row">
                                <div class="col-md-12">

                                    <label>Promotion Conditions:</label>
                                    <div class="title-underline"></div>

                                </div>

                            </div>
                            <div class="row">

                                <div class="col-md-12 text-right" style="margin-bottom: 5px !important;"><button class="btn btn-primary btn-circle" id="addrow-dis" type="button"><i class="fa fa-plus" aria-hidden="true"></i></button> </div>
                            </div>
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="table-responsive">

                                        <table class="table table-bordered" id="dis-table">
                                            <thead>
                                            <tr>
                                                <td>Minimum Order Amount</td>
                                                <td>Maximum Order Value</td>
                                                <td>Product</td>
                                                <td>Discount Percentage</td>
                                                <td>Discount Amount</td>
                                                <td>Applicable Days Time</td>

                                            </tr>
                                            </thead>
                                            <tbody id="dis-body">
                                            <tr>
                                                <td>
                                                    <div class="input-group">
                                                        <input type="text" name="minimum_order_amt_DCP[]" class="form-control">
                                                        <span class="input-group-addon">$</span>
                                                    </div>




                                                </td>
                                                <td>
                                                    <div class="input-group">
                                                        <input type="text" name="maximum_order_amt_DCP[]" class="form-control">
                                                        <span class="input-group-addon">$</span>
                                                    </div>


                                                </td>
                                                <td>

                                                    <select name="promotion_detail_product_DCP_0[]" class="form-control applicable_item" multiple  style="width:300px !important;">
                                                        <option selected>None</option>

                                                    </select>


                                                </td>
                                                <td>
                                                    <div class="input-group">
                                                        <input type="text" name="promotion_percentage_DCP[]" class="form-control">
                                                        <span class="input-group-addon">%</span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input-group">
                                                        <input type="text" name="promotion_fix_amt_DCP[]" class="form-control">
                                                        <span class="input-group-addon">$</span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div id="add-work-hour">

                                                        <table >
                                                            <tr>
                                                                <td>
                                                                    <table id="table-time">
                                                                        <tr>
                                                                            <td> <div class="input-group bootstrap-timepicker timepicker" style="width: 200px !important;">
                                                                                    <input  type="text" name="start_time_DCP[]" class="form-control input-small timepicker1" >
                                                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                                                                </div></td>
                                                                            <td> To </td>
                                                                            <td> <div class="input-group bootstrap-timepicker timepicker"  style="width: 200px !important;">
                                                                                    <input  type="text" name="end_time_DCP[]" class="form-control input-small timepicker2">
                                                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                                                                </div></td>
                                                                        </tr>
                                                                        <tr id="row-time">
                                                                            <td colspan="3">
                                                                                <ul class="days-week">
                                                                                    <input type="hidden" name="allcheck_DCP[]" class="allcheck">
                                                                                    <li><label><input type="checkbox" class="checkmon checkDay" name="check[]" style="display:none;"  value="Mon"><a onclick="dayofmon(this);">Mon</a></label></li>
                                                                                    <li><label><input type="checkbox" class="checktue checkDay" name="check[]" style="display:none;"  value="Tue"><a onclick="dayoftue(this);"> Tue</a></label></li>
                                                                                    <li><label><input type="checkbox" class="checkwed checkDay" name="check[]" style="display:none;"  value="Wed"><a onclick="dayofwed(this);"> Wed</a></label></li>
                                                                                    <li><label><input type="checkbox" class="checkthu checkDay" name="check[]" style="display:none;" value="Thu"><a onclick="dayofthu(this);"> Thu</a></label></li>
                                                                                    <li><label><input type="checkbox" class="checkfri checkDay" name="check[]" style="display:none;" value="Fri"><a onclick="dayoffri(this);"> Fri</a></label></li>
                                                                                    <li><label><input type="checkbox" class="checksat checkDay" name="check[]" style="display:none;" value="Sat"><a onclick="dayofsat(this);"> Sat</a></label></li>
                                                                                    <li><label><input type="checkbox" class="checksun checkDay" name="check[]" style="display:none;" value="Sun"><a onclick="dayofsun(this);"> Sun</a></label></li>


                                                                                </ul>
                                                                            </td>
                                                                        </tr>
                                                                    </table>




                                                                </td>


                                                            </tr>
                                                        </table>


                                                    </div>
                                                </td>

                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div id="buy_free"  style="display: none;">
                            <div class="row">
                                <div class="col-md-12">

                                    <label>Promotion Conditions:</label>
                                    <div class="title-underline"></div>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-md-12">
                                    <div class="table-responsive">

                                        <table class="table table-bordered" >
                                            <thead>
                                            <tr>
                                                <td>Minimum Order Amount</td>
                                                <td>Maximum Order Amount</td>
                                                <td>Product</td>
                                                <td>Free Quantity</td>
                                                <td>Free Item</td>
                                                <td>Applicable Days Times</td>

                                            </tr>
                                            </thead>
                                            <tbody id="pro-body">
                                            <tr>
                                                <td>
                                                    <div class="input-group">
                                                        <input type="text" name="minimum_order_amt_BGP" class="form-control">
                                                        <span class="input-group-addon">$</span>
                                                    </div>
                                                </td>
                                                <td>

                                                    <div class="input-group">
                                                        <input type="text" name="maximum_order_amt_BGP" class="form-control">
                                                        <span class="input-group-addon">$</span>
                                                    </div>



                                                </td>
                                                <td>
                                                    <select name="promotion_detail_product_BGP[]" class="form-control applicable_item" multiple  style="width:250px !important;">
                                                        <option selected>None</option>

                                                    </select>


                                                </td>
                                                <td>

                                                    <input type="text" name="promotion_free_qty_BGP" class="form-control">



                                                </td>
                                                <td>
                                                    <select name="promotion_free_item_BGP" class="form-control applicable_item"   style="width:250px !important;">
                                                        <option selected>None</option>

                                                    </select>


                                                </td>
                                                <td>
                                                    <div id="add-work-hour">

                                                        <table >
                                                            <tr>
                                                                <td>
                                                                    <table id="table-time">
                                                                        <tr>
                                                                            <td> <div class="input-group bootstrap-timepicker timepicker" style="width: 200px !important;">
                                                                                    <input  type="text" name="start_time_BGP" class="form-control input-small timepicker1" >
                                                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                                                                </div></td>
                                                                            <td> To </td>
                                                                            <td> <div class="input-group bootstrap-timepicker timepicker"  style="width: 200px !important;">
                                                                                    <input  type="text" name="end_time_BGP" class="form-control input-small timepicker2">
                                                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                                                                </div></td>
                                                                        </tr>
                                                                        <tr id="row-time">
                                                                            <td colspan="3">
                                                                                <ul class="days-week">
                                                                                    <input type="hidden" name="allcheck_BGP" class="allcheck_BGP">
                                                                                    <li><label><input type="checkbox" class="checkmon checkDayBGP" name="check[]" style="display:none;"  value="Mon"><a onclick="dayofmon(this);">Mon</a></label></li>
                                                                                    <li><label><input type="checkbox" class="checktue checkDayBGP" name="check[]" style="display:none;"  value="Tue"><a onclick="dayoftue(this);"> Tue</a></label></li>
                                                                                    <li><label><input type="checkbox" class="checkwed checkDayBGP" name="check[]" style="display:none;"  value="Wed"><a onclick="dayofwed(this);"> Wed</a></label></li>
                                                                                    <li><label><input type="checkbox" class="checkthu checkDayBGP" name="check[]" style="display:none;" value="Thu"><a onclick="dayofthu(this);"> Thu</a></label></li>
                                                                                    <li><label><input type="checkbox" class="checkfri checkDayBGP" name="check[]" style="display:none;" value="Fri"><a onclick="dayoffri(this);"> Fri</a></label></li>
                                                                                    <li><label><input type="checkbox" class="checksat checkDayBGP" name="check[]" style="display:none;" value="Sat"><a onclick="dayofsat(this);"> Sat</a></label></li>
                                                                                    <li><label><input type="checkbox" class="checksun checkDayBGP" name="check[]" style="display:none;" value="Sun"><a onclick="dayofsun(this);"> Sun</a></label></li>


                                                                                </ul>
                                                                            </td>
                                                                        </tr>
                                                                    </table>




                                                                </td>


                                                            </tr>
                                                        </table>


                                                    </div>
                                                </td>

                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-12 text-right"><button class="btn btn-default" type="submit">Save</button></div>
                        {!! Form::close() !!}
                    </div>

                </div>

            </div>

            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-md-12 -->

    </div>
    </div>

    @include('admin.include.popup-loading')
@stop
@section('style')
    {!! Html::style('vendors/bootstrap-timepicker/css/bootstrap-timepicker.css') !!}
@stop
@section('script')
    {!! Html::script('vendors/bootstrap-timepicker/js/bootstrap-timepicker.js') !!}
    {!! Html::script('js/upload-image.js') !!}
    <script>
        $('.applicable_item').select2();
        $('.timepicker1').timepicker();
        $('.timepicker2').timepicker();

        $('.start-date').datetimepicker({
            format: 'YYYY-MM-DD',
        });
        $('.end-date').datetimepicker({
        bannerrmat: 'YYYY-MM-DD',
        });

        $('#add-promotion').click(function(e){
            $('#promo-img').trigger('click');
        });
        $('#pro_type').change(function(){
            var type = $(this).val();
            if(type == 'DCP'){
                $("#discount").slideDown(1000);
                $("#credit_voucher").css('display','none');
                $("#allocate").css('display','none');
                $("#buy_free").css('display','none');
            }else if(type == 'BGP'){
                $("#discount").css('display','none');
                $("#allocate").css('display','none');
                $("#credit_voucher").css('display','none');
                $("#buy_free").slideDown(1000);
            }else if(type == 'CGP'){
                $("#discount").css('display','none');
                $("#buy_free").css('display','none');
                $("#allocate").css('display','block');
                $("#credit_voucher").slideDown(1000);
            }
        });
        var cu = 1;
        $('#addrow-cupon').click(function(){
            $('#cupon-table #cupon-body').append('<tr><td> <input type="text" name="minimum_order_amt_CGP[]" class="form-control"> </td>' +
                    '<td><input type="text" name="maximum_order_amt_CGP[]" class="form-control"></td><td>' +
                    '<select name="promotion_detail_product_CGP_'+cu+'[]" class="form-control applicable_item" multiple style="width:300px !important;">' +
                    '<option selected>None</option></select></td><td> <div id="add-work-hour"><table > <tr><td>' +
                    '<table id="table-time"><tr><td> <div class="input-group bootstrap-timepicker timepicker" style="width: 200px !important;">' +
                    '<input  type="text" name="start_time_CGP[]" class="form-control input-small timepicker1" >' +
                    '<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span></div></td>' +
                    '<td>To</td><td> <div class="input-group bootstrap-timepicker timepicker"  style="width: 200px !important;">' +
                    '<input  type="text" name="end_time_CGP[]" class="form-control input-small timepicker2">' +
                    '<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span></div></td>' +
                    '</tr><tr id="row-time"><td colspan="3"><ul class="days-week"><input type="hidden" name="allcheck_CGP[]" class="allcheck_CGP">' +
                    ' <li><label><input type="checkbox" class="checkmon checkDayCGP" name="check[]" style="display:none;"  value="Mon"><a onclick="dayofmon(this);">Mon</a></label></li>' +
                    ' <li><label><input type="checkbox" class="checktue checkDayCGP" name="check[]" style="display:none;"  value="Tue"><a onclick="dayoftue(this);"> Tue</a></label></li>' +
                    ' <li><label><input type="checkbox" class="checkwed checkDayCGP" name="check[]" style="display:none;"  value="Wed"><a onclick="dayofwed(this);"> Wed</a></label></li>' +
                    ' <li><label><input type="checkbox" class="checkthu checkDayCGP" name="check[]" style="display:none;" value="Thu"><a onclick="dayofthu(this);"> Thu</a></label></li>' +
                    ' <li><label><input type="checkbox" class="checkfri checkDayCGP" name="check[]" style="display:none;" value="Fri"><a onclick="dayoffri(this);"> Fri</a></label></li>' +
                    ' <li><label><input type="checkbox" class="checksat checkDayCGP" name="check[]" style="display:none;" value="Sat"><a onclick="dayofsat(this);"> Sat</a></label></li>' +
                    ' <li><label><input type="checkbox" class="checksun checkDayCGP" name="check[]" style="display:none;" value="Sun"><a onclick="dayofsun(this);"> Sun</a></label></li>' +
                    ' </ul></td></tr></table></td></tr> </table></div></td></tr>');
            $('.applicable_item').select2();
            $('.timepicker1').timepicker();
            $('.timepicker2').timepicker();
            cu++;
        });
        var d=1;
        $('#addrow-dis').click(function(){
            $('#dis-table #dis-body').append('<tr><td> <input type="text" name="minimum_order_amt_DCP[]" class="form-control"> </td>' +
                    '<td><input type="text" name="maximum_order_amt_DCP[]" class="form-control"></td><td>' +
                    '<select name="promotion_detail_product_DCP_'+d+'[]" class="form-control applicable_item" multiple style="width:300px !important;">' +
                    '<option selected>None</option></select></td><td><input type="text" name="promotion_percentage_DCP[]" class="form-control"></td><td><input type="text" name="promotion_fix_amt_DCP[]" class="form-control"></td><td> <div id="add-work-hour"><table > <tr><td>' +
                    '<table id="table-time"><tr><td> <div class="input-group bootstrap-timepicker timepicker" style="width: 200px !important;">' +
                    '<input  type="text" name="start_time_DCP[]" class="form-control input-small timepicker1" >' +
                    '<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span></div></td>' +
                    '<td>To</td><td> <div class="input-group bootstrap-timepicker timepicker"  style="width: 200px !important;">' +
                    '<input  type="text" name="end_time_DCP[]" class="form-control input-small timepicker2">' +
                    '<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span></div></td>' +
                    '</tr><tr id="row-time"><td colspan="3"><ul class="days-week"><input type="hidden" name="allcheck_DCP[]" class="allcheck">' +
                    ' <li><label><input type="checkbox" class="checkmon checkDay" name="check[]" style="display:none;"  value="Mon"><a onclick="dayofmon(this);">Mon</a></label></li>' +
                    ' <li><label><input type="checkbox" class="checktue checkDay" name="check[]" style="display:none;"  value="Tue"><a onclick="dayoftue(this);"> Tue</a></label></li>' +
                    ' <li><label><input type="checkbox" class="checkwed checkDay" name="check[]" style="display:none;"  value="Wed"><a onclick="dayofwed(this);"> Wed</a></label></li>' +
                    ' <li><label><input type="checkbox" class="checkthu checkDay" name="check[]" style="display:none;" value="Thu"><a onclick="dayofthu(this);"> Thu</a></label></li>' +
                    ' <li><label><input type="checkbox" class="checkfri checkDay" name="check[]" style="display:none;" value="Fri"><a onclick="dayoffri(this);"> Fri</a></label></li>' +
                    ' <li><label><input type="checkbox" class="checksat checkDay" name="check[]" style="display:none;" value="Sat"><a onclick="dayofsat(this);"> Sat</a></label></li>' +
                    ' <li><label><input type="checkbox" class="checksun checkDay" name="check[]" style="display:none;" value="Sun"><a onclick="dayofsun(this);"> Sun</a></label></li>' +
                    ' </ul></td></tr></table></td></tr> </table></div></td></tr>');
            $('.applicable_item').select2();
            $('.timepicker1').timepicker();
            $('.timepicker2').timepicker();
            d++;
        });
        $('body').on('click','.days-week input[type="checkbox"]',function(){
            var dayOfWeek = '';
            var parent = $(this).parents('.days-week');
            parent.find('.checkDay:checked').each(function(index,element){
                var checkBox = $(this);
                dayOfWeek += checkBox.val() + '|';

            });

            parent.find('.allcheck').val(dayOfWeek.slice(0,dayOfWeek.length -1));
        });
        $('body').on('click','.days-week input[type="checkbox"]',function(){
            var dayOfWeek = '';
            var parent = $(this).parents('.days-week');
            parent.find('.checkDayBGP:checked').each(function(index,element){
                var checkBox = $(this);
                dayOfWeek += checkBox.val() + '|';

            });

            parent.find('.allcheck_BGP').val(dayOfWeek.slice(0,dayOfWeek.length -1));
           // alert(dayOfWeek.slice(0,dayOfWeek.length -1));
        });
        $('body').on('click','.days-week input[type="checkbox"]',function(){
            var dayOfWeek = '';
            var parent = $(this).parents('.days-week');
            parent.find('.checkDayCGP:checked').each(function(index,element){
                var checkBox = $(this);
                dayOfWeek += checkBox.val() + '|';

            });

            parent.find('.allcheck_CGP').val(dayOfWeek.slice(0,dayOfWeek.length -1));
            // alert(dayOfWeek.slice(0,dayOfWeek.length -1));
        });
        function removePromotion(self){
            $("#banner-promotion").css('display','block');
            $(self).closest('td').remove();
        }
        function dayofmon(self){
            var check = $(self).closest("td").find(".checkmon").is(":checked");
            if(check){
                $(self).closest("li").removeClass('active');
            }else{
                $(self).closest("li").addClass('active');
            }

        }


        function dayoftue(self){
            var check = $(self).closest("td").find(".checktue").is(":checked");
            if(check){
                $(self).closest("li").removeClass('active');
            }else{
                $(self).closest("li").addClass('active');
            }

        }
        function dayofwed(self){
            var check = $(self).closest("td").find(".checkwed").is(":checked");
            if(check){
                $(self).closest("li").removeClass('active');
            }else{
                $(self).closest("li").addClass('active');
            }

        }
        function dayofthu(self){
            var check = $(self).closest("td").find(".checkthu").is(":checked");
            if(check){
                $(self).closest("li").removeClass('active');
            }else{
                $(self).closest("li").addClass('active');
            }

        }
        function dayoffri(self){
            var check = $(self).closest("td").find(".checkfri").is(":checked");
            if(check){
                $(self).closest("li").removeClass('active');
            }else{
                $(self).closest("li").addClass('active');
            }

        }
        function dayofsat(self){
            var check = $(self).closest("td").find(".checksat").is(":checked");
            if(check){
                $(self).closest("li").removeClass('active');
            }else{
                $(self).closest("li").addClass('active');
            }

        }
        function dayofsun(self){
            var check = $(self).closest("td").find(".checksun").is(":checked");
            if(check){
                $(self).closest("li").removeClass('active');
            }else{
                $(self).closest("li").addClass('active');
            }

        }

    </script>
@stop
