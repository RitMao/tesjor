@extends('admin.layout.layout')
@include('admin.layout.menu')
@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Create Restaurant Profile</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-8">Create Restaurant Profile</div>
                            <div class="col-md-4">
                                <div class="row pull-right" style="padding-right: 10px;">
                                <a href="#"><button class="btn btn-primary"> <i class="fa fa-eye" aria-hidden="true"></i>
                                 Web View</button></a>
                                   <button class="btn btn-primary"> <i class="fa fa-mobile" aria-hidden="true"></i>
                                            Mobile View</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel-heading -->
                    {!! Form::open(array('url' => route('post-admin-restaurant-profile'),'id' => 'frm-restaurant-profile')) !!}
                    <div class="panel-body">
                        @if(Session::has('success-message'))
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{Session::get('success-message')}}

                            </div>
                        @endif
                        @if(Session::has('error-message'))
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{Session::get('error-message')}}

                            </div>
                        @endif
                                <!-- Restaurant Detail -->
                                <div class="panel panel-Primary ">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-md-12">Restaurant Detail</div>
                                </div>
                            </div>
                            <div class="panel-body">
                                <!-- legal name -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label>Branch of:</label>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="form-group">
                                                    {!! Form::select('size',array('none' => 'None'), null, ['class' => 'form-control']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label>Legal Name:</label>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="form-group">
                                                    {!! Form::text('legal_name',null,array('class' => 'form-control','id' => 'legal_name' ,'required' )) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label>Restaurant Class:</label>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="form-group">
                                                    {!! Form::select('merchant_class_cd', $merchant_cd, null, ['class' => 'form-control','placeholder' => 'None']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label>Website:</label>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="form-group">
                                                    {!! Form::text('website',null,array('class' => 'form-control','id' => 'website' )) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label>Description:</label>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="form-group">
                                                    {{ Form::textarea('desc', null, ['cols' => '70','rows' => '7', 'class' => 'form-control']) }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label>Country</label>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="form-group">
                                                    {{--{{Form::select('country', $country, null, ['class' => 'form-control','id' => 'country'])}}--}}
                                                    <select name="country" class="form-control" id="country" required>
                                                        <option value=''>Select Country</option>
                                                        @foreach($country as $co)
                                                        <option value="{{$co['country_code']}}" data-id="{{$co['id']}}">{{$co['country_name']}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label>Capital/Province</label>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="form-group">
                                                    <div id="state-replace">
                                                        <select name="province" class="form-control" id="province" required>
                                                            <option value=''>Select Capital/Province</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label>City/District</label>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="form-group">
                                                    <div id="city-replace">
                                                        <select name="city" class="form-control"  id="city" required>
                                                            <option value=''>Select City/District</option>
                                                        </select>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-2">
                                                <label>Street Address</label>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="form-group">
                                                    {{Form::text('street',null,array('class' => 'form-control','required'))}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label>Email:</label>
                                            </div>
                                            <div class="col-md-10">

                                                <div class="form-group">
                                                    {!! Form::text('email',null,array('class' => 'form-control','id' => 'email' )) !!}
                                                    <span id="email_valid" style="color:red;"></span>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label>Phone Number:</label>
                                            </div>
                                            <div class="col-md-10">

                                                <div class="form-group">
                                                    <div class="input-group " >
                                                        <span class="input-group-addon code_group">+855</span>
                                                        {!! Form::hidden('code_phone',null,array('id' => 'code_phone'))!!}
                                                        {!! Form::text('phone_number',null,array('class' => 'form-control numeric','id' => 'phone_number' )) !!}
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-offset-2 col-md-3">
                                                <div class="form-group">
                                                    {{Form::text('latitude',null,array('class' => 'form-control','id' => 'lat','placeholder' => 'lattitude'))}}
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    {{Form::text('longitude',null,array('class' => 'form-control','id' => 'lng','placeholder' => 'longitude'))}}
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                   <button class="btn btn-primary" id="select-location" type="button"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                                        Select Location</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div id="map-canvas" style="width: 100%; height: 230px">
                                                </div>
                                                </div>
                                        </div>

                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-12">

                                        <label>Contact Person:</label>
                                        <div class="title-underline"></div>

                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-1">
                                        <label>First Name:</label>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            {{Form::text('first_name',null,array('class' => 'form-control', 'id' => 'name'))}}
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <label>Phone Number:</label>
                                    </div>
                                    <div class="col-md-5">

                                        <div class="input-group " >
                                            <span class="input-group-addon code_group">+855</span>
                                            {!! Form::hidden('code_con_phone',null,array('id' => 'code_con_phone'))!!}
                                            {!! Form::text('con_number',null,array('class' => 'form-control numeric','id' => 'con_phone_number' )) !!}
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-1">
                                        <label>Last Name:</label>
                                    </div>
                                    <div class="col-md-5">

                                        <div class="form-group">
                                            {{Form::text('last_name',null,array('class' => 'form-control', 'id' => 'name'))}}

                                        </div>

                                    </div>
                                    <div class="col-md-1">

                                        <label>Email:</label>


                                    </div>
                                    <div class="col-md-5">

                                        <div class="form-group">
                                            {{Form::text('email_contact',null,array('class' => 'form-control', 'id' => 'email_contact'))}}
                                            <span id="email_con_valid" style="color:red;"></span>
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">

                                        <label>Social Media:</label>
                                        <div class="title-underline"></div>

                                    </div>

                                </div>

                                <div class="row">

                                    @if(!empty($social))
                                        <?php $i = 1 ;?>
                                @foreach($social[0] as $key => $value)
                                    @if($value['is_active']==true)
                                                    <div class="loop_social">
                                                <div class="col-md-1">

                                                    <label>{{$value['item_value']}} page:</label>


                                                </div>
                                                <div class="col-md-5">

                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="page_link[]" id="page_link" class="all_social" >
                                                        <span class="pat_link" style="color:red;"></span>
                                                        <input type="hidden" class="form-control" name="item_code[]" id="item_code" value="{{$value['item_code']}}" >
                                                    </div>

                                                </div>
                                                        </div>

                                @if(($i%2) == 0)
                                </div>
                                <div class="row">
                                    @endif

                                            <?php $i++;?>
                                    @endif
                                @endforeach
                            @endif
                            </div></div>
                            </div>
                            <!-- Media management-->
                            <div class="panel panel-Primary ">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-md-12">Media Management</div>
                                    </div>
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Logo</label>
                                            <div class="title-underline"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="photo-manage">
                                            <table id="logo-table">
                                                <tr>

                                                </tr>
                                            </table>
                                        </div>
                                        <div class="upload-manage" id="logo-picture">
                                            <input type="hidden"  id="type_logo" name="type_logo" value="PLG">
                                            <input type="file" class="v-hidden" id="logo" name="logo">
                                            <i class="fa fa-plus-circle  fa-5x img-add-circle" aria-hidden="true" id="add-logo" style="cursor: pointer;"></i>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">

                                            <label>Banner</label>
                                            <div class="title-underline"></div>

                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="photo-manage">
                                            <table id="banner-table">
                                                <tr>


                                                </tr>
                                            </table>
                                        </div>
                                        <div class="upload-manage" id="banner-picture">
                                            <input type="file" class="v-hidden" id="banner" name="banner">
                                            <i class="fa fa-plus-circle  fa-5x img-add-circle" aria-hidden="true" id="add-banner" style="cursor: pointer;"></i>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">

                                            <label>Photo Gallery</label>
                                            <div class="title-underline"></div>

                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="photo-manage">
                                            <table id="photos-upload">
                                                <tr>


                                                </tr>
                                            </table>

                                        </div>

                                        <div class="upload-manage" id="photos-picture">
                                            <input type="file" accept="image/gif, image/jpeg, image/png" class="v-hidden" name='photos[]' id="photos" multiple>
                                            <i class="fa fa-plus-circle  fa-5x img-add-circle" aria-hidden="true" id="add-photos"  style="cursor: pointer;"></i>
                                        </div>

                                    </div>


                                    <div class="row">
                                        <div class="col-md-12">

                                            <label>360 degree Video</label>
                                            <div class="title-underline"></div>

                                        </div>

                                    </div>
                                    <div class="row">

                                        <div class="photo-manage">
                                            <table>
                                                <tr>
                                                    {{--<td>--}}
                                                        {{--<div class="video-upload">--}}
                                                            {{--<iframe id="video" width="300" height="230" src="" frameborder="0" allowfullscreen></iframe>--}}
                                                            {{--<i class="fa fa-trash fa-2x icon-pop" id="remove-video-degree"></i>--}}
                                                        {{--</div>--}}
                                                    {{--</td>--}}


                                                </tr>
                                            </table>
                                        </div>


                                        <div class="upload-manage">
                                            <input type="file" class="v-hidden">
                                            <i class="fa fa-plus-circle  fa-5x img-add-circle" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">

                                            <label>Videos</label>
                                            <div class="title-underline"></div>

                                        </div>

                                    </div>
                                    <div class="row">

                                        <div class="photo-manage">
                                            <table>
                                                <tr>
                                                    {{--<td>--}}
                                                        {{--<div class="video-upload">--}}
                                                            {{--<iframe id="video" width="300" height="230" src="" frameborder="0" allowfullscreen></iframe>--}}
                                                            {{--<a href="#" class="icon-pop" onclick="removeVideo(this);">X</a>--}}

                                                        {{--</div>--}}
                                                    {{--</td>--}}
                                                    {{--<td>--}}
                                                        {{--<div class="video-upload">--}}
                                                            {{--<iframe id="video" width="300" height="230" src="" frameborder="0" allowfullscreen></iframe>--}}
                                                            {{--<a href="#" class="icon-pop" onclick="removeVideo(this);">X</a>--}}
                                                        {{--</div>--}}
                                                    {{--</td>--}}

                                                </tr>
                                            </table>
                                        </div>


                                        <div class="upload-manage">
                                            <input type="file" class="v-hidden">
                                            <i class="fa fa-plus-circle  fa-5x img-add-circle" aria-hidden="true" ></i>
                                        </div>

                                    </div>


                                </div>
                            </div>

                            <!-- Operation-->
                            <div class="panel panel-Primary ">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-md-12">Operation</div>
                                    </div>
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">

                                    <div class="row">
                                        <div class="col-sm-1 col-md-1 col-lg-1">
                                            <label>Working hours:</label>
                                        </div>

                                        <div class="col-sm-4 col-md-4 col-lg-4">
                                            <div id="add-work-hour">
                                                <div class="table-responsive">
                                                            <div class="work-hour" >

                                                                <table style="border-collapse: separate; border-spacing: 5px;">
                                                                    <tr>
                                                                        <td>
                                                                            <table id="table-time">
                                                                                <tr>
                                                                                    <td> <div class="input-group bootstrap-timepicker timepicker" style="width: 200px !important;">
                                                                                            <input  type="text" name="start_time[]" class="form-control input-small timepicker1" >

                                                                                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>

                                                                                        </div></td>
                                                                                    <td>To</td>
                                                                                    <td> <div class="input-group bootstrap-timepicker timepicker"  style="width: 200px !important;">
                                                                                            <input  type="text" name="end_time[]" class="form-control input-small timepicker2">
                                                                                            <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>

                                                                                        </div></td>
                                                                                </tr>
                                                                                <tr id="row-time">
                                                                                    <td colspan="3">
                                                                                        <ul class="days-week">
                                                                                            <input type="hidden" name="allcheck[]" class="allcheck">
                                                                                            <li><label><input type="checkbox" class="checkmon checkDay" name="check[]" style="display:none;"  value="Mon"><a onclick="dayofmon(this);">Mon</a></label></li>
                                                                                            <li><label><input type="checkbox" class="checktue checkDay" name="check[]" style="display:none;"  value="Tue"><a onclick="dayoftue(this);"> Tue</a></label></li>
                                                                                            <li><label><input type="checkbox" class="checkwed checkDay" name="check[]" style="display:none;"  value="Wed"><a onclick="dayofwed(this);"> Wed</a></label></li>
                                                                                            <li><label><input type="checkbox" class="checkthu checkDay" name="check[]" style="display:none;" value="Thu"><a onclick="dayofthu(this);"> Thu</a></label></li>
                                                                                            <li><label><input type="checkbox" class="checkfri checkDay" name="check[]" style="display:none;" value="Fri"><a onclick="dayoffri(this);"> Fri</a></label></li>
                                                                                            <li><label><input type="checkbox" class="checksat checkDay" name="check[]" style="display:none;" value="Sat"><a onclick="dayofsat(this);"> Sat</a></label></li>
                                                                                            <li><label><input type="checkbox" class="checksun checkDay" name="check[]" style="display:none;" value="Sun"><a onclick="dayofsun(this);"> Sun</a></label></li>


                                                                                        </ul>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>




                                                                        </td>


                                                                    </tr>
                                                                </table>
                                                            </div>
                                                    </div>
                                                    </div>
                                            </div>
                                        <div class="col-sm-3 col-md-3 col-lg-3"><button class="btn btn-primary btn-circle" id="add-work" type="button"><i class="fa fa-plus" aria-hidden="true"></i>
                                               </button> </div>
                                        </div>

                                    <!-- Cuisine  -->
                                    <div class="row">
                                        <div class="col-md-1">
                                            <label>Cuisine:</label>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                {!! Form::select('cuisine[]',$cuisine_cd,null,array('class' => 'form-control','id' => 'cuisine','multiple','style' =>'max-width: 450px;')) !!}

                                            </div>
                                        </div>
                                    </div>
                                    <!-- Service offer -->
                                    <div class="row">
                                        <div class="col-md-1">
                                            <label>Service offer:</label>
                                        </div>
                                        <div class="col-md-11">
                                            <div class="row">
                                                <?php $k = 1; ?>
                                                @foreach($tran_cd[0] as $key => $value)
                                                  @if($value['is_active']==true)
                                                <div class="col-md-3">
                                                    <input type="checkbox" name="tran_type_cd[]" value="{{$value['item_code']}}"> {{$value['item_value']}}
                                                </div>
                                                    @if(($k%2)==0)
                                                      </div><div class="row">
                                                        @endif
                                                    <?php $k++;?>
                                                  @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Tax -->
                                    <div class="row">
                                        <div class="col-md-1">
                                            <label>Tax:</label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="checkbox" name="tax"> Include VAT
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Commission Section-->
                            <div class="panel panel-Primary ">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-md-12">Commission</div>
                                    </div>
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">

                                    <div id="add-commission">
                                        <div class="row">
                                            <div class="col-md-1">
                                                <label>Name:</label>
                                            </div>
                                            <div class="col-md-2">
                                               <input type="text" name="commis_name" class="form-control">
                                            </div>
                                            <div class="col-md-1">
                                                <label>Start Date:</label>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="input-group">
                                                    <input id="star-date" name="start_date" type="text" class="date-picker form-control start-date" value="{{ Carbon\Carbon::today()->format('Y-m-d')}}" />
                                                    <label class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>

                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <label>End Date:</label>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                <div class="input-group">
                                                    <input id="en-date"  name="end_date"  type="text" class="date-picker form-control end-date" value="{{ Carbon\Carbon::today()->format('Y-m-d')}}" />
                                                    <label class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>

                                                    </label>
                                                </div>
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-1">
                                                <label>Description:</label>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                {{ Form::textarea('commis_desc', null, ['cols' => '70','rows' => '9', 'class' => 'form-control']) }}
                                                </div>
                                                </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-9 text-right" style="margin-bottom: 5px !important;"><button class="btn btn-primary btn-circle" id="addrow-com" type="button"><i class="fa fa-plus" aria-hidden="true"></i></button> </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-1">
                                                <label>Detail</label>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="table-responsive">

                                                    <table class="table table-bordered" id="commis-table">
                                                        <thead>
                                                          <tr>
                                                            <td>Min Sell Amount</td>
                                                            <td>Max Sell Amount</td>
                                                            <td>Percentage</td>
                                                            <td>Fix Amount</td>
                                                            <td>Min Commission Amount</td>
                                                            <td>Max Commission Amount</td>

                                                         </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <div class="input-group">
                                                                    <input type="text" name="min_sell_amt[]" class="form-control">
                                                                    <span class="input-group-addon">$</span>
                                                                </div>

                                                            </td>
                                                            <td>
                                                                <div class="input-group">
                                                                    <input type="text" name="max_sell_amt[]" class="form-control">
                                                                    <span class="input-group-addon">$</span>
                                                                </div>

                                                            </td>
                                                            <td>
                                                                <div class="input-group">
                                                                    <input type="text" name="commis_percentage[]" class="form-control">
                                                                    <span class="input-group-addon">%</span>
                                                                </div>

                                                            </td>
                                                            <td>
                                                                <div class="input-group">
                                                                    <input type="text" name="commis_fix_amt[]" class="form-control">
                                                                    <span class="input-group-addon">$</span>
                                                                </div>

                                                            </td>

                                                            <td>
                                                                <div class="input-group">
                                                                    <input type="text" name="min_commission_amt[]" class="form-control">
                                                                    <span class="input-group-addon">$</span>
                                                                </div>

                                                            </td>
                                                            <td>
                                                                <div class="input-group">
                                                                    <input type="text" name="max_commission_amt[]" class="form-control">
                                                                    <span class="input-group-addon">$</span>
                                                                </div>

                                                            </td>

                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12 text-right"><button class="btn btn-default" type="submit">Save</button> <button class="btn btn-default">Cancel</button></div>

                    </div>


                    </div>

                    {!! Form::close() !!}
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->

        </div>
    </div>
    @include('admin.include.popup-upload-banner')
    @include('admin.include.popup-loading')
@stop
@section('style')
    {!! Html::style('vendors/bootstrap-timepicker/css/bootstrap-timepicker.css') !!}
@stop
@section('script')
    {!! Html::script('vendors/bootstrap-timepicker/js/bootstrap-timepicker.js') !!}
    {!! Html::script('js/upload-image.js') !!}
    {!! Html::script('js/numeric.js') !!}
    <script>
        $(document).ready(function(){

            $('.timepicker1').timepicker();
            $('.timepicker2').timepicker();
            $('.numeric').ForceNumeric();

            $('.start-date').datetimepicker({
                format: 'YYYY-MM-DD',
            });
            $('.end-date').datetimepicker({
                format: 'YYYY-MM-DD',
            });

            $("#email").change(function(){
                var email = $(this).val();
               if(!isValidEmailAddress(email)){
                   $("#email_valid").html('Invalid email');
                   $(this).css('border-color','red');
               }else{
                   $("#email_valid").html('');
                   $(this).css('border-color','');
               }
            });
            $("#email_contact").change(function(){
                var email = $(this).val();
                if(!isValidEmailAddress(email)){
                    $("#email_con_valid").html('Invalid email');
                    $(this).css('border-color','red');
                }else{
                    $("#email_con_valid").html('');
                    $(this).css('border-color','');
                }
            });
            $("#frm-restaurant-profile").validate({

                submitHandler: function (form) {


                    form.submit();

                }

            });
            $("#country").change(function(){
                var id = $(this).find('option:selected').attr('data-id');
                $.ajax({
                    url: baseUrl + '/get-state',
                    type: "GET",
                    data: {'id': id},
                    dataType:"json",
                    success:function(data){
                        console.log(data.phone_code);
                        $("#state-replace").html(data.state);
                        $(".code_group").html(data.phone_code);
                        $("#code_con_phone").val(data.phone_code);
                        $("#code_phone").val(data.phone_code);
                    }

                });
            });
            $('body').on('change', '#province', function(){
                var id = $(this).find('option:selected').attr('data-id');
                var country_id =  $(this).find('option:selected').attr('data-country-id');
                $.ajax({
                    url: baseUrl + '/get-city',
                    type: "GET",
                    data: {'id': id,'country_id':country_id},
                    dataType:"html",
                    success:function(data){
                        $("#city-replace").html(data);
                    }

                });
           });
            $("#validate-form").validate({

                rules: {
                    firstname: "required",
                    lastname: "required",
                    email: {
                        required: true,
                        email: true //email is required AND must be in the form of a valid email address
                    },
                    password: {
                        required: true,
                        minlength: 6
                    }
                },

//specify validation error messages
                messages: {
                    firstname: "First Name field cannot be blank!",
                    lastname: "Last Name field cannot be blank!",
                    password: {
                        required: "Password field cannot be blank!",
                        minlength: "Your password must be at least 6 characters long"
                    },
                    email: "Please enter a valid email address"
                },

                submitHandler: function(form){
                    form.submit();
                }

            });
            clearAllText();
        });

        $('#cuisine').select2();
        $('#addrow-com').click(function(){
            $('#commis-table tbody').append('<tr> <td>Min Sell Amount</td><td>Max Sell Amount</td><td>Percentage</td>' +
                    '<td>Fix Amount</td><td>Min Commission Amount</td><td>Max Commission Amount</td></tr></thead><tbody><tr><td><div class="input-group">' +
                    '<input type="text" name="min_sell_amt[]" class="form-control"><span class="input-group-addon">$</span></div></td><td><div class="input-group">' +
                    '<input type="text" name="max_sell_amt[]" class="form-control"><span class="input-group-addon">$</span></div></td>' +
                    '<td><div class="input-group"><input type="text" name="commis_percentage[]" class="form-control"> <span class="input-group-addon">%</span></div>' +
                    '</td> <td><div class="input-group"><input type="text" name="commis_fix_amt[]" class="form-control"> <span class="input-group-addon">$</span></div> ' +
                    '</td><td><div class="input-group"> <input type="text" name="min_commission_amt[]" class="form-control">  <span class="input-group-addon">$</span></div>' +
                    '</td><td> <div class="input-group"><input type="text" name="max_commission_amt[]" class="form-control"><span class="input-group-addon">$</span></div>' +
                    '</td></tr>'

            );
            $('.start-date').datetimepicker({
                format: 'YYYY-MM-DD',
            });
            $('.end-date').datetimepicker({
                format: 'YYYY-MM-DD',
            });
        });
        var add = 1;
        $("#add-work").click(function(){

            $("#add-work-hour").append(' <div class="table-responsive"><div class="work-hour" > <div class="btn-remove-row"><button class="btn btn-danger btn-circle" onclick="removeWork(this);" type="button"><i class="fa fa-minus" aria-hidden="true"></i></button> </div><table style="border-collapse: separate; border-spacing: 5px;">' +
                    '<tr><td> <table id="table-time"><tr><td> <div class="input-group bootstrap-timepicker timepicker" style="width: 200px !important;">' +
                    '<input  type="text" name="start_time[]" class="form-control input-small timepicker1" ><span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>' +
                    '</div></td>' +
                    '<td>To</td>' +
                    '<td> <div class="input-group bootstrap-timepicker timepicker"  style="width: 200px !important;">' +
                    '<input  type="text" name="end_time[]" class="form-control input-small timepicker2">' +
                    ' <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>' +
                    '</div></td></tr>' +
                    '<tr id="row-time"><td colspan="3"> ' +
                    '<ul class="days-week"> <input type="hidden" name="allcheck[]" class="allcheck"> <li><label><input type="checkbox" class="checkmon checkDay" name="check[]" style="display: none;"   value="Mon"><a onclick="dayofmon(this);">Mon</a></label></li>' +
                    ' <li><label><input type="checkbox" class="checktue checkDay" name="check[]"  style="display: none;" value="Tue"><a onclick="dayoftue(this);"> Tue</a></label></li>' +
                    ' <li><label><input type="checkbox" class="checkwed checkDay" name="check[]"  style="display: none;" value="Wed"><a onclick="dayofwed(this);"> Wed</a></label></li>' +
                    ' <li><label><input type="checkbox" class="checkthu checkDay" name="check[]"  style="display: none;"  value="Thu"><a onclick="dayofthu(this);"> Thu</a></label></li>' +
                    ' <li><label><input type="checkbox" class="checkfri checkDay" name="check[]"   style="display: none;"  value="Fri"><a onclick="dayoffri(this);"> Fri</a></label></li>' +
                    ' <li><label><input type="checkbox" class="checksat checkDay" name="check[]"  style="display: none;"  value="Sat"><a onclick="dayofsat(this);"> Sat</a></label></li>' +
                    ' <li><label><input type="checkbox" class="checksun checkDay" name="check[]"  style="display: none;"  value="Sun"><a onclick="dayofsun(this);"> Sun</a></label></li>' +

                    '</td></tr> </table></td></tr> </table> </div></div>');
            $('.timepicker1').timepicker();
            $('.timepicker2').timepicker();



        add++;

        });
        $('body').on('click','.days-week input[type="checkbox"]',function(){
            var dayOfWeek = '';
            var parent = $(this).parents('.days-week');
            parent.find('.checkDay:checked').each(function(index,element){
                var checkBox = $(this);
                dayOfWeek += checkBox.val() + '|';

            });

            parent.find('.allcheck').val(dayOfWeek.slice(0,dayOfWeek.length -1));
        });

        $('body').on('change','.loop_social input[type="text"]',function(){
            var parent = $(this).parents('.loop_social');

            var urlPattern = new RegExp("(http|ftp|https)://([\w.,@?^=%&amp;:/~+#-]*[\w@?^=%&amp;/~+#-])?");
            var text = $(this).val();
            if(text!=''){
                if(urlPattern.test(text)){
                    parent.find('.pat_link').html('');
                    $(this).css('border-color','');
                }else{
                    parent.find('.pat_link').html('Invalid URL');
                    $(this).css('border-color','red');
                }
            }else{
                parent.find('.pat_link').html('');
                $(this).css('border-color','');
            }

        });

        function isValidEmailAddress(emailAddress) {
            var pattern = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return pattern.test($.trim(emailAddress));
        };
        function removeWork(self){
            $(self).closest('.work-hour').remove();

        }

        function removeImage(self){
            $(self).closest('td').remove();

        }
        function removeLogo(self){
            $(self).closest('td').remove();
            $('#logo-picture').css('display','block');

        }
        function removeBanner(self){
            $(self).closest('td').remove();
            $('#banner-picture').css('display','block');

        }
        function removeVideo(self){
            $(self).closest('td').remove();
        }

        function dayofmon(self){
            var check = $(self).closest("td").find(".checkmon").is(":checked");
            if(check){
                $(self).closest("li").removeClass('active');
            }else{
                $(self).closest("li").addClass('active');
            }

        }


        function dayoftue(self){
            var check = $(self).closest("td").find(".checktue").is(":checked");
            if(check){
                $(self).closest("li").removeClass('active');
            }else{
                $(self).closest("li").addClass('active');
            }

        }
        function dayofwed(self){
            var check = $(self).closest("td").find(".checkwed").is(":checked");
            if(check){
                $(self).closest("li").removeClass('active');
            }else{
                $(self).closest("li").addClass('active');
            }

        }
        function dayofthu(self){
            var check = $(self).closest("td").find(".checkthu").is(":checked");
            if(check){
                $(self).closest("li").removeClass('active');
            }else{
                $(self).closest("li").addClass('active');
            }

        }
        function dayoffri(self){
            var check = $(self).closest("td").find(".checkfri").is(":checked");
            if(check){
                $(self).closest("li").removeClass('active');
            }else{
                $(self).closest("li").addClass('active');
            }

        }
        function dayofsat(self){
            var check = $(self).closest("td").find(".checksat").is(":checked");
            if(check){
                $(self).closest("li").removeClass('active');
            }else{
                $(self).closest("li").addClass('active');
            }

        }
        function dayofsun(self){
            var check = $(self).closest("td").find(".checksun").is(":checked");
            if(check){
                $(self).closest("li").removeClass('active');
            }else{
                $(self).closest("li").addClass('active');
            }

        }


        $("#select-location").click(function(){
            var lat = $("#lat").val();
            var lng = $("#lng").val();
            initialize(lat,lng);
            google.maps.event.addDomListener(window, 'load', initialize);
        });

        function initialize(lat,lng) {
            if(lat == '' && lng == ''){
                var latitude = 11.5500;
                var longitude = 104.9167;
            }else{
                var latitude = lat;
                var longitude = lng;
            }

            var zoom = 15;

            var LatLng = new google.maps.LatLng(latitude, longitude);
            document.getElementById('lat').value = latitude;
            document.getElementById('lng').value = longitude;
            var mapProp = {
                center: LatLng,
                zoom:12,
                mapTypeId:google.maps.MapTypeId.ROADMAP
            };
            var map=new google.maps.Map(document.getElementById("map-canvas"), mapProp);
            var marker = new google.maps.Marker({
                position: LatLng,
                map: map,
                title: 'Drag Me!',
                draggable: true
            });

            var searchbox = new google.maps.places.SearchBox(document.getElementById('searchmap'));
            google.maps.event.addListener(searchbox,'places_changed',function(){
                var places = searchbox.getPlaces();
                var bounds = new google.maps.LatLngBounds();
                var i,place;
                for(i=0;place=places[i];i++){
                    bounds.extend(place.geometry.location);
                    marker.setPosition(place.geometry.location);
                    document.getElementById('lat').value = place.geometry.location.lat();
                    document.getElementById('lng').value = place.geometry.location.lng();
                }
                map.fitBounds(bounds);
                map.setZoom(15);
            });
            google.maps.event.addListener(marker, 'dragend', function(event) {

                document.getElementById('lat').value = event.latLng.lat();
                document.getElementById('lng').value = event.latLng.lng();



            });
            google.maps.event.addListener(marker, 'places_changed', function(event) {

                document.getElementById('lat').value = event.latLng.lat();
                document.getElementById('lng').value = event.latLng.lng();



            });
        }


    </script>
    {!! Html::script('js/gmap.js') !!}
@stop