@extends('admin.layout.layout')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Sign In</h3>
                    </div>
                    <div class="panel-body">
                      @if(Session::has('error-message'))
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           {{Session::get('error-message')}}

                        </div>
                       @endif
                        {!! Form::open(array('url' => route('post-admin-login'))) !!}

                            <fieldset>
                                <div class="form-group">
                                    {{Form::text('email',null,array('class' => 'form-control','placeholder' => 'E-mail','autofocus','required')) }}
                                    {{--<input class="form-control" placeholder="E-mail" name="email" type="email" autofocus>--}}
                                </div>
                                <div class="form-group">
                                    {{Form::password('password',array('class' => 'form-control','placeholder' => 'Password','required')) }}

                                </div>
                                <div class="checkbox">
                                    <label>
                                       {{Form::checkbox('remember', 'remember')}}Remember Me
                                    </label>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->

                                {{Form::submit('Login',array('class' => 'btn btn-lg btn-success btn-block'))}}
                            </fieldset>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
