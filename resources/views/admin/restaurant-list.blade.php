@extends('admin.layout.layout')
@include('admin.layout.menu')
@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Restaurant List</h1>
        </div>
        @if(Session::has('error-message'))
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {{Session::get('error-message')}}

            </div>
            @endif
            @if(Session::has('message'))
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {{Session::get('message')}}

            </div>
            @endif
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-8">Restaurant List</div>
                        <div class="col-md-4">
                            <div class="row pull-right">
                               <button class="btn btn-default" onclick="checkDelete();"> <i class="fa fa-trash" aria-hidden="true"></i>
                                        Delete</button>
                               <a href="{{route('admin-restaurant-profile')}}"><button class="btn btn-default"> <i class="fa fa-plus" aria-hidden="true"></i>
                                        Add New</button></a>
                                <button class="btn btn-default"> <i class="fa fa-refresh" aria-hidden="true"></i>
                                        Refresh List</button>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                        <div class="dataTable_wrapper">
                            <table width="100%" class="table table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th><input type="checkbox" id="checkAll"/> Check all</th>
                                    <th>Restaurant ID</th>
                                    <th>Restaurant Name</th>
                                    <th>Address</th>
                                    <th>Contact</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!empty($data))
                                    @foreach($data as $value)
                                <tr class="odd gradeX">
                                    <td class="text_center"><input type="checkbox" class="check_id" value="{{$value['id']}}"></td>
                                    <td class="text_center">ID-{{$value['id']}}</td>
                                    <td class="font-text"><a href="{{route('update-restaurant-list',$value['id'])}}">{{$value['legal_name']}}</a></td>
                                    <td class="font-text">{{$value['street_address']}}</td>
                                    <td class="font-text">{{$value['phone']}}</td>
                                    <td>
                                        @foreach($status as $sta_cd)
                                        @if($sta_cd['item_code']==$value['status_cd'])
                                            {{$sta_cd['item_value']}}
                                        @endif
                                        @endforeach
                                    </td>
                                    <td ><a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a>&nbsp&nbsp<a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a> &nbsp&nbsp<a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a> </td>
                                </tr>
                                     @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
</div>
@stop
@section('script')
    <script>
        $(document).ready(function() {
            $('#dataTables-example').DataTable({

                "columnDefs": [ {
                    "targets": [0,6],
                    "orderable": false
                } ]
            });

        });
        function checkDelete(){
            var check = $(".check_id").is(":checked");
            if(check){
                var checkid = [];

                $('.check_id:checked').each(function () {
                    var id = $(this).val();
                    checkid.push(id);
                });
                console.log(checkid);
//                $.ajax({
//                    url: baseUrl + '/admin/reject/photo',
//                    type: "POST",
//                    data: {
//                        'id': checkid
//                    },
//                    error: function () {
//
//                        hideLoadingPop();
//                    },
//                    success: function (data) {
//                    }
//                });
            }else{
                alert('please check with data! ');
                return;
            }
        }
        $("#checkAll").change(function () {
    $("input:checkbox").prop('checked', $(this).prop("checked"));
});
    </script>
@stop