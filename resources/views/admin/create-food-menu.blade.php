@extends('admin.layout.layout')
@include('admin.layout.menu')
@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Create Food Menu</h1>
            </div>

            <!-- /.col-lg-12 -->
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-12">Create Food Menu </div>

                        </div>
                    </div>
                    <!-- /.panel-heading -->
                    {!! Form::open(array('url' => route('add-food-item'),'id' => 'frm-food-item')) !!}
                    <div class="panel-body">
                        @if(Session::has('success-message'))
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{Session::get('success-message')}}

                            </div>
                        @endif
                        @if(Session::has('error-message'))
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {{Session::get('error-message')}}

                            </div>
                        @endif

                         {{Form::hidden('res_id',$res_id)}}
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="row">
                                     <div class="col-lg-2">
                                         <label>Name</label>
                                     </div>
                                     <div class="col-lg-10">

                                         <div class="form-group">
                                        {{ Form::text('food_menu',null,array('class' => 'form-control')) }}
                                             </div>
                                    </div>
                                </div>

                                <div class="row" >
                                    <div class="col-lg-2">
                                       <label>Ingredient</label> 
                                    </div>
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                     {{ Form::text('ingredient',null,array('class' => 'form-control')) }}
                                       </div>

                                    </div>
                                </div>

                                <div class="row" >
                                    <div class="col-lg-2">
                                       <label>Ingredient</label> 
                                    </div>
                                    <div class="col-lg-10">

                                    </div>
                                </div>

                                <div class="row" >
                                    <div class="col-lg-2">
                                      <label>Description</label>  
                                    </div>
                                    <div class="col-lg-10">

                                        {{ Form::textarea('desc', null, ['cols' => '40','rows' => '6', 'class' => 'form-control']) }}

                                    </div>
                                </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="row">

                            <div class="col-md-offset-2 col-md-10">
                                <table id="menu-table" style="width: 100% !important;">
                                    <tr>
                                        <td>

                                        </td>

                                    </tr>
                                </table>
                                <div class="upload-image-promotion" id="menu-promotion" >
                                    <input accept="image/gif, image/jpeg, image/png" type="file" class="v-hidden" id="menu-img" name="menu_img">
                                    <i class="fa fa-plus-circle fa-5x img-add-circle" aria-hidden="true" id="add-menu" style="cursor: pointer;"></i>
                                </div>
                                <input type="hidden" id="link">
                                <div class="error" id="pic-error"></div>
                            </div>

                        </div>
    
                         </div>
                        
                    <div class="col-lg-6">
                         
                                   <div class="col-lg-8">
                                      <label> Size</label>  

                                   </div>
                                    <div class="title-underline col-lg-6"></div> 
                                <div class="row" style="margin-bottom: 10px;">
                                    <div class="col-lg-offset-2 col-lg-8">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i>
                                        </span>
                                            <!-- {{ Form::text('price_search',null,array('class' => 'form-control','id' => 'price-search')) }} -->
                                           <!--  <select class="from-control price-search" id="size">
                                            @if(!empty($size_cd))
                                                @foreach($size_cd as $key => $value)
                                                <option value="{{$key}}" data-value="{{$value}}">{{$value}}</option>

                                                @endforeach
                                            @endif
                                            </select> -->

{!! Form::select('price_search[]',$size_cd,null,array('class' => 'form-control price-search','id' => 'size','style' =>'max-width: 450px;')) !!}

                                        </div>
                                        <div class="error" id="size-error"></div>
                                    </div>
                                    <div class="col-lg-2">
                                        <button class="btn btn-primary" type="button" id="price-add">Add</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-offset-2 col-lg-10">

                                       <table class="table table-bordered" id="price-table">
                                           <thead>
                                                <tr>
                                                    <td>Default</td>
                                                    <td>Size</td>
                                                    <td>Price</td>
                                                </tr>

                                            </thead>
                                           <tbody>
                                              
                                           </tbody>
                                       </table>

                                    </div>
                                </div>                               
                             
                         </div>
                               <div class="col-lg-6">
                                <div class="row" >
                                    <div class="col-lg-8">
                                        <label>Cooking Preference </label>
                                    </div>
                                    
                                </div>
                                <div class="title-underline col-lg-6"></div> 
                                  <div class="row" style="margin-bottom: 10px;">
                                    <div class="col-lg-offset-2 col-lg-8">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i>
</span>
                                            <!-- {{ Form::text('cook_search',null,array('class' => 'form-control','id' => 'cook-search')) }} -->
                                            {!! Form::select('cook_search[]',$preference_cd,null,array('class' => 'form-control cook-search','id' => 'preference','style' =>'max-width: 450px;')) !!}


                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <button class="btn btn-primary" type="button" id="cook-add">Add</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <table class="table table-bordered" id="cook-table">
                                            <thead>
                                            <tr>
                                                <td>Default</td>
                                                <td>Preference</td>

                                            </tr>

                                            </thead>
                                            <tbody>
                                      
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                               </div>

                               </div>

                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <label> AddOn </label>
                                    </div>
                                    
                                </div>  
                                <div class="title-underline col-lg-6"></div>                               
                                <div class="row" style="margin-bottom: 10px;">
                                    <div class="col-lg-offset-2 col-lg-8">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i>
</span>
{!! Form::select('price_search[]',$addon_cd,null,array('class' => 'form-control manda-search','id' => 'addOn')) !!}
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <button class="btn btn-primary" type="button" id="manda-add">Add</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <table class="table table-bordered" id="manda-table">
                                            <thead>
                                            <tr>
                                                <!-- <td>Mandatory</td> -->
                                                <td>Applicable</td>
                                                <td>Price</td>
                                            </tr>

                                            </thead>
                                            <tbody>
                                            
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-10">
                                        <label>  Extra</label>
                                    </div>
                                  
                                </div>
                                <div class="title-underline col-lg-6"></div> 
                                <div class="row" style="margin-bottom: 10px;">
                                    <div class="col-lg-offset-2 col-lg-8">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i>
</span>
                                            <!-- {{ Form::text('extra_search',null,array('class' => 'form-control','id' => 'extra-search')) }}
                                             -->


{!! Form::select('extra_search[]',$extra_cd,null,array('class' => 'form-control extra-search','id' => 'extra')) !!}
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <button class="btn btn-primary" type="button" id="extra-add">Add</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <table class="table table-bordered" id="extra-table">
                                            <thead>
                                            <tr>
                                                <!-- <td>Mandatory</td> -->
                                                <td>Extra </td>
                                                <td>Price</td>

                                            </tr>

                                            </thead>
                                            <tbody>
                                            
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div> 
                            <div class="col-lg-12">                        
                            <div class="col-lg-6">             
                                <div class="row">
                                    <div class="col-lg-2">
                                       <label>Meal times</label> 
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="form-group">
                                              {!! Form::select('meal[]',$meal_cd,null,array('class' => 'form-control','id' => 'meal','multiple','required')) !!}
                                               <label for="meal" class="error" id="meal-error"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                <div class="col-lg-6">
                                 <div class="row">
                                        <div class="col-lg-2">
                                            <label>Cuisine</label>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                                {!! Form::select('cuisine[]',$cuisine_cd,null,array('class' => 'form-control','id' => 'cuisine','multiple')) !!}
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <div>
                            
                        <div class="col-lg-12 text-right"><button class="btn btn-default" type="submit">Save</button></div>
                        {!! Form::close() !!}
                        </div>

                    </div>
                        
                    </div>

                </div>

                        <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->

    </div>
    </div>

    @include('admin.include.popup-loading')
@stop
@section('style')
    {!! Html::style('vendors/bootstrap-timepicker/css/bootstrap-timepicker.css') !!}
@stop
@section('script')
    {!! Html::script('vendors/bootstrap-timepicker/js/bootstrap-timepicker.js') !!}
    {!! Html::script('js/upload-image.js') !!}
    <script>

        $(document).ready(function(){


            $('#meal').select2();
            $('#food').select2();
            $('#cuisine').select2();
            $('#size').select2();
            $('#addOn').select2();
            $('#extra').select2();
            $('#preference').select2();

            $("#link").val('');
            $("#frm-food-item").validate({
                rules:{
                    food_menu:"required",
                },

                submitHandler: function (form) {

                    var rowCount = $('#price-table tr').length;
                  //  alert(rowCount);
                    if(rowCount == 1){

                        $("#size-error").html('Please add size price');
                        return;
                    }
                    if($("#link").val() == ''){

                        $("#pic-error").html('Please upload picture');
                        return;
                    }

                   form.submit();

                }

            });




            var array_size = [];
            var s = 1;

            var i = 0;
            $("#price-add").click(function(){


                var search = $(".price-search").val();

                var serch_name = $(".price-search").find("option:selected").text();


                if(jQuery.inArray(search, array_size)!= '-1') {

                    alert("Size have already!")
                    return false;

                } else {
                    if(s==1){
                        array_size.push(search);
                        $('#price-table tbody').append('<tr><td><input type="radio" name="size_radio" value="'+search+'" checked></td><td>'+serch_name+'</td>' +

                                '<td><input type="hidden" name="code_size[]" value="'+search+'"> <input type="text" name="size_price['+i+']" id="size_price_'+search+'" class="form-control" required> ' +
                                '</td></tr>');
                    }else{
                        array_size.push(search);
                        $('#price-table tbody').append('<tr><td><input type="radio" name="size_radio" value="'+search+'"></td><td>'+serch_name+'</td>' +
                                '<td><input type="hidden" name="code_size[]" value="'+search+'"> <input type="text" name="size_price['+i+']" id="size_price_'+search+'" class="form-control" required> ' +
                                '</td></tr>');
                    }



                }


                s++;
                i++;

            });
            var array_cook = [];
            var c = 1;

            $("#cook-add").click(function(){
                var search = $(".cook-search").val();

                var serch_name = $(".cook-search").find("option:selected").text();

                if(jQuery.inArray(search, array_cook)!= '-1') {


                    alert("Cook have already!")
                    return false;

                } else {
                    if(c==1){
                        array_cook.push(search);
                        $('#cook-table tbody').append('<tr><td><input type="radio" name="cook_radio" value="'+search+'" checked ></td><td>'+serch_name+'</td></tr>');
                    }else{
                        array_cook.push(search);
                        $('#cook-table tbody').append('<tr><td><input type="radio" name="cook_radio" value="'+search+'" ></td><td>'+serch_name+'</td></tr>');
                    }


                }
            c++;
            });
            var array_manda = [];
            var m = 0;

            $("#manda-add").click(function(){
                var search = $(".manda-search").val();

                var serch_name = $(".manda-search").find("option:selected").text();

                if(jQuery.inArray(search, array_manda)!= '-1') {


                    alert("Addon have already!")

                    return false;

                } else {
                    array_manda.push(search);

                    $('#manda-table tbody').append('<tr><td>'+serch_name+'</td><td><input type="hidden" name="code_addon[]" value="'+search+'" class="form-control"><input type="text" name="price_addon['+m+']" class="form-control" required></td></tr>');

                }
             m++;
            });
            var array_extra = [];
            var e = 0;

            $("#extra-add").click(function(){
                var search = $(".extra-search").val();

                var serch_name = $(".extra-search").find("option:selected").text();

                if(jQuery.inArray(search, array_extra)!= '-1') {


                    alert("Extra have already!")

                    return false;

                } else {
                    array_extra.push(search);

                    $('#extra-table tbody').append('<tr><td>'+serch_name+'</td><td><input type="hidden" name="code_extra[]" value="'+search+'" class="form-control"><input type="text" name="price_extra['+e+']" class="form-control" required></td></tr>');

                }
                e++;

            });
            $("#add-menu").click(function(){
                $("#menu-img").trigger('click');
            });


        });

        function removeMenu(self){
            $(self).closest('td').remove();
            $("#menu-promotion").css('display','block');
            $("#link").val('');
        }


    </script>
@stop