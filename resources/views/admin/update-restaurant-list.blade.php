@extends('admin.layout.layout')
@include('admin.layout.menu')
@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page-header">Update Restaurant Detail</h1>
            </div>

            <!-- /.col-lg-12 -->
        </div>
        @if($status==true)
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-8">Update Restaurant Detail</div>
                            <div class="col-md-4">
                                <div class="row pull-right">

                                    <a href="#"><button class="btn btn-primary"> <i class="fa fa-eye" aria-hidden="true"></i>

                                            Web View</button></a>
                                    <button class="btn btn-primary"> <i class="fa fa-mobile" aria-hidden="true"></i>

                                        Mobile View</button>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#profile" data-toggle="tab">Restaurant Profile</a>
                            </li>
                            <li><a href="#promotion" data-toggle="tab">Promotion</a>
                            </li>
                            <li><a href="#foodmenu" data-toggle="tab">Food Menu</a>
                            </li>
                            <li><a href="#user" data-toggle="tab">User</a>
                            </li>
                            <li><a href="#order" data-toggle="tab">Order Configuration</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="profile">
                                <!-- Restaurant Detail -->
                                {!! Form::open(array('url' => route('post-update-restaurant-list'),'id' => 'frm-restaurant-profile')) !!}
                                <div class="panel panel-Primary ">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-md-12">Restaurant Detail</div>


                                        </div>
                                    </div>

                                    <div class="panel-body">
                                        <!-- legal name -->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-2">

                                                        <label>Branch of:</label>


                                                    </div>
                                                    <div class="col-md-10">
                                                        <div class="form-group">
                                                            {!! Form::select('size',array('none' => 'None'), null, ['class' => 'form-control']) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-2">

                                                        <label>Legal Name:</label>


                                                    </div>
                                                    <div class="col-md-10">
                                                        <div class="form-group">
                                                            {!! Form::text('legal_name',$data['legal_name'],array('class' => 'form-control','id' => 'legal_name' )) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-2">

                                                        <label>Restaurant Class:</label>


                                                    </div>
                                                    <div class="col-md-10">
                                                        <div class="form-group">
                                                            {!! Form::select('merchant_class_cd', $merchant_cd, $data['merchant_class_cd'], ['class' => 'form-control','placeholder' => 'None']) !!}
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-2">

                                                        <label>Website:</label>


                                                    </div>
                                                    <div class="col-md-10">

                                                        <div class="form-group">
                                                            {!! Form::text('website',$data['website'],array('class' => 'form-control','id' => 'website' )) !!}

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-2">

                                                        <label>Description:</label>


                                                    </div>
                                                    <div class="col-md-10">

                                                        <div class="form-group">
                                                            {{ Form::textarea('desc', $data['about'], ['cols' => '70','rows' => '7', 'class' => 'form-control']) }}

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>



                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <label>Country</label>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <div class="form-group">
                                                            {{Form::select('country', array('KH' => 'Cambodia'), $data['country_code'], ['class' => 'form-control','id' => 'country'])}}

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <label>Capital/Province</label>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <div class="form-group">
                                                            {{Form::select('province', array('PNH' => 'Phnom Penh'), $data['state_code'], ['class' => 'form-control','placeholder' => 'Select Capital/Province'])}}


                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <label>City/District</label>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <div class="form-group">
                                                            {{Form::select('city', array('pp' => 'Phnom Penh'), null, ['class' => 'form-control','placeholder' => 'Select City/District'])}}
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <label>Street Address</label>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <div class="form-group">
                                                            {{Form::text('street',$data['street_address'],array('class' => 'form-control'))}}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-2">

                                                        <label>Email:</label>


                                                    </div>
                                                    <div class="col-md-10">

                                                        <div class="form-group">
                                                            {!! Form::text('email',$data['email'],array('class' => 'form-control','id' => 'email' )) !!}
                                                            <span id="email_valid" style="color:red;"></span>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-2">

                                                        <label>Phone Number:</label>


                                                    </div>
                                                    <div class="col-md-10">
                                                        <?php

                                                        //$phone_number = explode(" ",$data['phone']);

                                                        ?>
                                                        <div class="form-group">
                                                            <div class="input-group " >
                                                                <span class="input-group-addon" id="code_group">{{$data['phone']}}</span>
                                                                {!! Form::hidden('code_phone',$data['phone'],array('id' => 'code_phone'))!!}
                                                                {!! Form::text('phone_number',$data['phone'],array('class' => 'form-control','id' => 'phone_number' )) !!}
                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-offset-2 col-md-3">
                                                        <div class="form-group">
                                                            {{Form::text('latitude',$data['latitude'],array('class' => 'form-control','id' => 'lat','placeholder' => 'lattitude'))}}

                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            {{Form::text('longitude',$data['longitude'],array('class' => 'form-control','id' => 'lng','placeholder' => 'longitude'))}}

                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <button class="btn btn-primary" id="select-location" type="button"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                                                Select Location</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div id="map-canvas" style="width: 100%; height: 230px">


                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">

                                                <label>Contact Person:</label>
                                                <div class="title-underline"></div>

                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-1">

                                                <label>First Name:</label>


                                            </div>
                                            <div class="col-md-5">
                                                 <?php
                                                    if(empty($data['contact'])){
                                                        $firstname ='';
                                                    }else{
                                                        $firstname = $data['contact'][0]['first_name'];
                                                    }
                                                 ?>
                                                <div class="form-group">
                                                    {{Form::text('first_name',$firstname,array('class' => 'form-control', 'id' => 'name'))}}

                                                </div>

                                            </div>
                                            <div class="col-md-1">

                                                <label>Phone Number:</label>


                                            </div>
                                            <div class="col-md-5">
                                                <?php
                                                if(empty($data['contact'])){
                                                    $phone_contact ='';
                                                }else{
                                                    $phone_contact = $data['contact'][0]['phone_number'];
                                                }
                                                ?>
                                                <div class="input-group " >
                                                    <span class="input-group-addon" id="code_group"></span>
                                                    {!! Form::hidden('code_con_phone',$phone_contact,array('id' => 'code_con_phone'))!!}
                                                    {!! Form::text('con_number',$phone_contact,array('class' => 'form-control','id' => 'con_phone_number' )) !!}
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-1">

                                                <label>Last Name:</label>


                                            </div>
                                            <div class="col-md-5">
                                                <?php
                                                if(empty($data['contact'])){
                                                    $lastname ='';
                                                }else{
                                                    $lastname = $data['contact'][0]['last_name'];
                                                }
                                                ?>
                                                <div class="form-group">
                                                    {{Form::text('last_name',$lastname,array('class' => 'form-control', 'id' => 'name'))}}

                                                </div>

                                            </div>
                                            <div class="col-md-1">

                                                <label>Email:</label>


                                            </div>
                                            <div class="col-md-5">
                                                <?php
                                                if(empty($data['contact'])){
                                                    $email ='';
                                                }else{
                                                    $email = $data['contact'][0]['email'];
                                                }
                                                ?>
                                                <div class="form-group">
                                                    {{Form::text('email_contact',$email,array('class' => 'form-control', 'id' => 'email_contact'))}}
                                                    <span id="email_con_valid" style="color:red;"></span>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">

                                                <label>Social Media:</label>
                                                <div class="title-underline"></div>

                                            </div>

                                        </div>

                                        <div class="row">

                                            @if(!empty($social))
                                                <?php $i = 1 ;?>
                                                @foreach($social[0] as $key => $value)
                                                    @if($value['is_active']==true)
                                                        <div class="loop_social">
                                                            <div class="col-md-1">

                                                                <label>{{$value['item_value']}} page:</label>


                                                            </div>
                                                            <div class="col-md-5">

                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" name="page_link[]" id="page_link" class="all_social" >
                                                                    <span class="pat_link" style="color:red;"></span>
                                                                    <input type="hidden" class="form-control" name="item_code[]" id="item_code" value="{{$value['item_code']}}" >
                                                                </div>

                                                            </div>
                                                        </div>

                                                        @if(($i%2) == 0)
                                        </div>
                                        <div class="row">
                                            @endif

                                            <?php $i++;?>
                                            @endif
                                            @endforeach
                                            @endif
                                        </div></div>



                                </div>


                                <!-- Media management-->
                                <div class="panel panel-Primary ">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-md-12">Media Management</div>


                                        </div>
                                    </div>
                                    <!-- /.panel-heading -->
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">

                                                <label>Logo</label>
                                                <div class="title-underline"></div>

                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="photo-manage">
                                                <table id="logo-table">
                                                    <tr>
                                                        @if($data['logo']['full_url']!='')
                                                        <td>
                                                            <div class="img-upload" id="logo-empty">
                                                                <img src="{{$data['logo']['full_url']}}" class="image-size" id="img-logo" />
                                                                <input type="hidden" name="logo_id" id="logo_id" value="{{$data['logo']['id']}}">
                                                                <input type="hidden" name="logo_title" id="logo_title" value="{{$data['logo']['title']}}">
                                                                <input type="hidden" name="logo_filename" id="logo_filename" value="">
                                                                <input type="hidden" name="logo_link" id="logo_link" value="{{$data['logo']['full_url']}}">
                                                                <a class="icon-pop" onclick="removeLogo(this);">X</a>
                                                            </div>
                                                        </td>
                                                        @endif
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="upload-manage" id="logo-picture" style="display:none;">
                                                <input type="hidden"  id="type_logo" name="type_logo" value="PLG">
                                                <input type="file" class="v-hidden" id="logo" name="logo">
                                                <i class="fa fa-plus-circle  fa-5x img-add-circle" aria-hidden="true" id="add-logo" style="cursor: pointer;"></i>
                                            </div>
                                            @if($data['logo']['full_url']=='')
                                            <div class="upload-manage" id="update-logo-picture">
                                                <input type="hidden"  id="type_logo" name="type_logo" value="PLG">
                                                <input type="file" class="v-hidden" id="update-logo" name="logo">
                                                <i class="fa fa-plus-circle  fa-5x img-add-circle" aria-hidden="true" id="update-add-logo" style="cursor: pointer;"></i>
                                            </div>
                                            @endif
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">

                                                <label>Banner</label>
                                                <div class="title-underline"></div>

                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="photo-manage">
                                                <table id="banner-table">
                                                    <tr>
                                                        @if($data['banner']['full_url']!='')
                                                        <td>
                                                            <div class="banner-upload" id="banner-empty">
                                                                <img src="{{$data['banner']['full_url']}}" class="banner-size" id="img-banner" />
                                                                <input type="hidden" name="banner_id" id="banner_id" value="{{$data['banner']['full_url']}}">
                                                                <input type="hidden" name="banner_title" id="banner_title" value="Merchant_banner">
                                                                <input type="hidden" name="banner_filename" id="banner_filename" value="">
                                                                <input type="hidden" name="banner_link" id="banner_link" value="{{$data['banner']['full_url']}}">
                                                                <a class="icon-banner" onclick="removeBanner(this);">X</a>

                                                            </div>
                                                        </td>
                                                        @endif
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="upload-manage" id="banner-picture" style="display:none;">
                                                <input type="file" class="v-hidden" id="banner" name="banner">
                                                <i class="fa fa-plus-circle  fa-5x img-add-circle" aria-hidden="true" id="add-banner" style="cursor: pointer;"></i>
                                            </div>
                                            @if($data['banner']['full_url']=='')
                                            <div class="upload-manage" id="update-banner-picture">
                                                <input type="file" class="v-hidden" id="update-banner" name="banner">
                                                <i class="fa fa-plus-circle  fa-5x img-add-circle" aria-hidden="true" id="update-add-banner" style="cursor: pointer;"></i>
                                            </div>
                                            @endif
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">

                                                <label>Photo Gallery</label>
                                                <div class="title-underline"></div>

                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="photo-manage">
                                                <table id="photos-upload">
                                                    <tr>
                                                        @foreach($data['photo_gallery'] as $photo)
                                                        <td>
                                                            <div class="img-upload">
                                                                <img src="{{$photo['full_url']}}" class="image-size" />
                                                                <a class="icon-pop" onclick="removeImage(this);">X</a>
                                                                <input type="hidden" name="img_filename[]" value="">
                                                                <input type="hidden" name="img_id[]" value="{{$photo['id']}}">
                                                                <input type="hidden" name="img_link[]" value="{{$photo['full_url']}}">
                                                                <input type="hidden" name="img_title[]" value="{{$photo['title']}}">
                                                            </div>

                                                        </td>
                                                        @endforeach
                                                    </tr>
                                                </table>

                                            </div>

                                            <div class="upload-manage" id="photos-picture">
                                                <input type="file" accept="image/gif, image/jpeg, image/png" class="v-hidden" name='photos[]' id="photos" multiple>
                                                <i class="fa fa-plus-circle  fa-5x img-add-circle" aria-hidden="true" id="add-photos"  style="cursor: pointer;"></i>
                                            </div>

                                        </div>


                                        <div class="row">
                                            <div class="col-md-12">

                                                <label>360 degree Video</label>
                                                <div class="title-underline"></div>

                                            </div>

                                        </div>
                                        <div class="row">

                                            <div class="photo-manage">
                                                <table>
                                                    <tr>
                                                        {{--<td>--}}
                                                        {{--<div class="video-upload">--}}
                                                        {{--<iframe id="video" width="300" height="230" src="" frameborder="0" allowfullscreen></iframe>--}}
                                                        {{--<i class="fa fa-trash fa-2x icon-pop" id="remove-video-degree"></i>--}}
                                                        {{--</div>--}}
                                                        {{--</td>--}}


                                                    </tr>
                                                </table>
                                            </div>


                                            <div class="upload-manage">
                                                <input type="file" class="v-hidden">
                                                <i class="fa fa-plus-circle  fa-5x img-add-circle" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">

                                                <label>Videos</label>
                                                <div class="title-underline"></div>

                                            </div>

                                        </div>
                                        <div class="row">

                                            <div class="photo-manage">
                                                <table>
                                                    <tr>
                                                        {{--<td>--}}
                                                        {{--<div class="video-upload">--}}
                                                        {{--<iframe id="video" width="300" height="230" src="" frameborder="0" allowfullscreen></iframe>--}}
                                                        {{--<a href="#" class="icon-pop" onclick="removeVideo(this);">X</a>--}}

                                                        {{--</div>--}}
                                                        {{--</td>--}}
                                                        {{--<td>--}}
                                                        {{--<div class="video-upload">--}}
                                                        {{--<iframe id="video" width="300" height="230" src="" frameborder="0" allowfullscreen></iframe>--}}
                                                        {{--<a href="#" class="icon-pop" onclick="removeVideo(this);">X</a>--}}
                                                        {{--</div>--}}
                                                        {{--</td>--}}

                                                    </tr>
                                                </table>
                                            </div>


                                            <div class="upload-manage">
                                                <input type="file" class="v-hidden">
                                                <i class="fa fa-plus-circle  fa-5x img-add-circle" aria-hidden="true" ></i>
                                            </div>

                                        </div>


                                    </div>
                                </div>

                                <!-- Operation-->
                                <div class="panel panel-Primary ">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-md-12">Operation</div>
                                        </div>
                                    </div>
                                    <!-- /.panel-heading -->
                                    <div class="panel-body">

                                        <div class="row">
                                            <div class="col-md-1">
                                                <label>Working hours:</label>
                                            </div>

                                            <div class="col-md-4">
                                                <div id="add-work-hour">

                                                    @foreach($data['working_hour'] as $work)
                                                        <?php $ex_days = explode('|',$work['days']); ?>
                                                        <?php $hours = explode('-',$work['hours']) ?>

                                                    <div class="work-hour" >

                                                        <table style="border-collapse: separate; border-spacing: 5px;">
                                                            <tr>
                                                                <td>
                                                                    <table id="table-time">
                                                                        <tr>
                                                                            <td> <div class="input-group bootstrap-timepicker timepicker" style="width: 200px !important;">
                                                                                    <input  type="text" name="start_time[]" value="{{$hours[0]}}"  class="form-control input-small timepicker1" >

                                                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>

                                                                                </div></td>
                                                                            <td>To</td>
                                                                            <td> <div class="input-group bootstrap-timepicker timepicker"  style="width: 200px !important;">
                                                                                    <input  type="text" name="end_time[]" value="{{$hours[1]}}" class="form-control input-small timepicker2">
                                                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>

                                                                                </div></td>
                                                                        </tr>

                                                                        <tr id="row-time">
                                                                            <td colspan="3">
                                                                                <ul class="days-week">
                                                                                    <input type="hidden" name="allcheck[]" class="allcheck" value="{{$work['days']}}">
                                                                                   <li <?php echo (in_array('Mon',$ex_days)?"class='active'":'')?>><label><input type="checkbox" class="checkmon checkDay" name="check[]" style="display:none;"  value="Mon"  <?php echo (in_array('Mon',$ex_days)?"checked":'')?>><a onclick="dayofmon(this);">Mon</a></label></li>
                                                                                    <li <?php echo (in_array('Tue',$ex_days)?"class='active'":'')?>><label><input type="checkbox" class="checktue checkDay" name="check[]" style="display:none;"  value="Tue" <?php echo (in_array('Tue',$ex_days)?"checked":'')?>><a onclick="dayoftue(this);"> Tue</a></label></li>
                                                                                    <li <?php echo (in_array('Wed',$ex_days)?"class='active'":'')?>><label><input type="checkbox" class="checkwed checkDay" name="check[]" style="display:none;"  value="Wed" <?php echo (in_array('Wed',$ex_days)?"checked":'')?>><a onclick="dayofwed(this);"> Wed</a></label></li>
                                                                                    <li <?php echo (in_array('Thu',$ex_days)?"class='active'":'')?>><label><input type="checkbox" class="checkthu checkDay" name="check[]" style="display:none;" value="Thu"  <?php echo (in_array('Thu',$ex_days)?"checked":'')?>><a onclick="dayofthu(this);"> Thu</a></label></li>
                                                                                    <li <?php echo (in_array('Fri',$ex_days)?"class='active'":'')?>><label><input type="checkbox" class="checkfri checkDay" name="check[]" style="display:none;" value="Fri"  <?php echo (in_array('Fri',$ex_days)?"checked":'')?>><a onclick="dayoffri(this);"> Fri</a></label></li>
                                                                                    <li <?php echo (in_array('Sat',$ex_days)?"class='active'":'')?>><label><input type="checkbox" class="checksat checkDay" name="check[]" style="display:none;" value="Sat"  <?php echo (in_array('Sat',$ex_days)?"checked":'')?>><a onclick="dayofsat(this);"> Sat</a></label></li>
                                                                                    <li <?php echo (in_array('Sun',$ex_days)?"class='active'":'')?>><label><input type="checkbox" class="checksun checkDay" name="check[]" style="display:none;" value="Sun"  <?php echo (in_array('Sun',$ex_days)?"checked":'')?>><a onclick="dayofsun(this);"> Sun</a></label></li>


                                                                                </ul>
                                                                            </td>
                                                                        </tr>
                                                                    </table>




                                                                </td>


                                                            </tr>
                                                        </table>
                                                    </div>
                                                    @endforeach
                                                </div>

                                            </div>

                                            <div class="col-md-3"><button class="btn btn-primary btn-circle" id="add-work" type="button"><i class="fa fa-plus" aria-hidden="true"></i>
                                                </button> </div>
                                        </div>

                                        <!-- Cuisine  -->
                                        <div class="row">
                                            <div class="col-md-1">
                                                <label>Cuisine:</label>
                                            </div>
                                            <div class="col-md-4">
                                                <?php $cuisine = explode('|',$data['list_menu_category_cd']);

                                                ?>
                                                <div class="form-group">
                                                    {{--{!! Form::select('cuisine[]',$cuisine_cd,$cuisine[0],array('class' => 'form-control','id' => 'cuisine','multiple','style' =>'max-width: 450px;')) !!}--}}
                                                    <select name="cuisine[]" class="form-control" id="cuisine" multiple style="max-width: 450px;">
                                                        @foreach($cuisine_cd as $key => $value)
                                                            <option value="{{$key}}"  <?php echo (in_array($key,$cuisine)?'selected':'')?>>{{$value}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Service offer -->
                                        <div class="row">
                                            <div class="col-md-1">
                                                <label>Service offer:</label>
                                            </div>
                                            <div class="col-md-11">
                                                <div class="row">

                                                    <?php $k = 1;
                                                          $tran = array();
                                                        foreach($data['transaction_type'] as $code){
                                                            array_push($tran,$code['transaction_type_cd']);
                                                        }
                                                    ?>



                                                    @foreach($tran_cd[0] as $key => $value)
                                                        @if($value['is_active']==true)
                                                            <div class="col-md-3">
                                                                <input type="checkbox" name="tran_type_cd[]" value="{{$value['item_code']}}" <?php echo (in_array($value['item_code'],$tran)?'checked':'')?>> {{$value['item_value']}}
                                                            </div>
                                                            @if(($k%2)==0)
                                                </div><div class="row">
                                                    @endif
                                                    <?php $k++;?>
                                                    @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Tax -->
                                        <div class="row">
                                            <div class="col-md-1">
                                                <label>Tax:</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="checkbox" name="tax" <?php echo ($data['is_include_tax']==1?'checked':'') ?>> Include VAT
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Commission Section-->
                                <div class="panel panel-Primary ">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-md-12">Commission</div>
                                        </div>
                                    </div>
                                    <!-- /.panel-heading -->
                                    <div class="panel-body">

                                        <div id="add-commission">
                                            <div class="row">
                                                <div class="col-md-1">
                                                    <label>Name:</label>
                                                </div>
                                                <?php
                                                    if(!empty($data['commission'])){
                                                       $com_name =  $data['commission'][0]['commission_name'];
                                                        $start_date = date("Y-m-d", strtotime($data['commission'][0]['start_date']));
                                                        $end_date = date("Y-m-d", strtotime($data['commission'][0]['end_date']));
                                                        $com_desc = $data['commission'][0]['description'];
                                                    }else{
                                                        $com_name = '';
                                                        $start_date = '';
                                                        $end_date = '';
                                                        $com_desc = '';
                                                    }
                                                ?>
                                                <div class="col-md-2">
                                                    <input type="text" value="{{$com_name}}" name="commis_name" class="form-control">
                                                </div>
                                                <div class="col-md-1">
                                                    <label>Start Date:</label>
                                                </div>
                                                <div class="col-md-2">

                                                    <div class="input-group">
                                                        <input id="star-date" name="start_date" type="text" class="date-picker form-control start-date" value="{{ $start_date }}" />
                                                        <label class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>

                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <label>End Date:</label>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input id="en-date"  name="end_date"  type="text" class="date-picker form-control end-date" value="{{ $end_date }}" />
                                                            <label class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>

                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-1">
                                                    <label>Description:</label>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        {{ Form::textarea('commis_desc', $com_desc , ['cols' => '70','rows' => '9', 'class' => 'form-control']) }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-9 text-right" style="margin-bottom: 5px !important;"><button class="btn btn-primary btn-circle" id="addrow-com" type="button"><i class="fa fa-plus" aria-hidden="true"></i></button> </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-1">
                                                    <label>Detail</label>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="table-responsive">

                                                        <table class="table table-bordered" id="commis-table">
                                                            <thead>
                                                            <tr>
                                                                <td>Min Sell Amount</td>
                                                                <td>Max Sell Amount</td>
                                                                <td>Percentage</td>
                                                                <td>Fix Amount</td>
                                                                <td>Min Commission Amount</td>
                                                                <td>Max Commission Amount</td>

                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @if(!empty($data['commission']))
                                                            @foreach($data['commission'][0]['commission_detail'] as $detail)
                                                            <tr>
                                                                <td>
                                                                    <div class="input-group">
                                                                        <input type="text" name="min_sell_amt[]" class="form-control" value="{{$detail['minimum_sell_amt']}}">
                                                                        <span class="input-group-addon">$</span>
                                                                    </div>

                                                                </td>
                                                                <td>
                                                                    <div class="input-group">
                                                                        <input type="text" name="max_sell_amt[]" class="form-control" value="{{$detail['maximum_sell_amt']}}">
                                                                        <span class="input-group-addon">$</span>
                                                                    </div>

                                                                </td>
                                                                <td>
                                                                    <div class="input-group">
                                                                        <input type="text" name="commis_percentage[]" class="form-control" value="{{$detail['commission_percentage']}}">
                                                                        <span class="input-group-addon">%</span>
                                                                    </div>

                                                                </td>
                                                                <td>
                                                                    <div class="input-group">
                                                                        <input type="text" name="commis_fix_amt[]" class="form-control" value="{{$detail['commission_fix_amt']}}">
                                                                        <span class="input-group-addon">$</span>
                                                                    </div>

                                                                </td>

                                                                <td>
                                                                    <div class="input-group">
                                                                        <input type="text" name="min_commission_amt[]" class="form-control" value="{{$detail['minimum_commission_amt']}}">
                                                                        <span class="input-group-addon">$</span>
                                                                    </div>

                                                                </td>
                                                                <td>
                                                                    <div class="input-group">
                                                                        <input type="text" name="max_commission_amt[]" class="form-control" value="{{$detail['maximum_commission_amt']}}">
                                                                        <span class="input-group-addon">$</span>
                                                                    </div>

                                                                </td>

                                                            </tr>
                                                                @endforeach
                                                                @else
                                                                <tr>
                                                                    <td>
                                                                        <div class="input-group">
                                                                            <input type="text" name="min_sell_amt[]" class="form-control" >
                                                                            <span class="input-group-addon">$</span>
                                                                        </div>

                                                                    </td>
                                                                    <td>
                                                                        <div class="input-group">
                                                                            <input type="text" name="max_sell_amt[]" class="form-control" >
                                                                            <span class="input-group-addon">$</span>
                                                                        </div>

                                                                    </td>
                                                                    <td>
                                                                        <div class="input-group">
                                                                            <input type="text" name="commis_percentage[]" class="form-control" >
                                                                            <span class="input-group-addon">%</span>
                                                                        </div>

                                                                    </td>
                                                                    <td>
                                                                        <div class="input-group">
                                                                            <input type="text" name="commis_fix_amt[]" class="form-control" >
                                                                            <span class="input-group-addon">$</span>
                                                                        </div>

                                                                    </td>

                                                                    <td>
                                                                        <div class="input-group">
                                                                            <input type="text" name="min_commission_amt[]" class="form-control" >
                                                                            <span class="input-group-addon">$</span>
                                                                        </div>

                                                                    </td>
                                                                    <td>
                                                                        <div class="input-group">
                                                                            <input type="text" name="max_commission_amt[]" class="form-control" >
                                                                            <span class="input-group-addon">$</span>
                                                                        </div>

                                                                    </td>

                                                                </tr>
                                                                @endif
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12 text-right"><button class="btn btn-default" type="submit">Save</button> <button class="btn btn-default">Cancel</button></div>
                                {!! Form::close() !!}
                            </div>
                            <div class="tab-pane fade in" id="promotion">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-md-8">Promotion List</div>
                                            <div class="col-md-4">
                                                <div class="row pull-right" style="padding-right: 10px;">
                                                    <button class="btn btn-default"> <i class="fa fa-trash" aria-hidden="true"></i>
                                                        Delete</button>
                                                    <a href="{{route('admin-get-promotion',$res_id)}}"><button class="btn btn-default" type="button"> <i class="fa fa-plus" aria-hidden="true"></i>
                                                            Add New</button></a>
                                                    <button class="btn btn-default"> <i class="fa fa-refresh" aria-hidden="true"></i>
                                                        Refresh List</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- /.panel-heading -->
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <div class="dataTable_wrapper">
                                                <table width="100%" class="table table-striped table-bordered table-hover" >
                                                    <thead>
                                                    <tr>
                                                        <th>Select</th>
                                                        <th>Promotion Type</th>
                                                        <th>Description</th>
                                                        <th>Promotion Start date/time</th>
                                                        <th>Promotion End date/time</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr class="odd gradeX">
                                                        <td><input type="checkbox"></td>
                                                        <td>ID-xxx</td>
                                                        <td>Item 1</td>

                                                        <td>S-$2.00 || M-$3.00 || L-$4.00</td>
                                                        <td><input type="checkbox"></td>
                                                        <td ><a href="#"><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
                                                            </a>&nbsp&nbsp<a href="#"><i class="fa fa-trash fa-2x" aria-hidden="true"></i></a>  </td>
                                                    </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- /.table-responsive -->

                                    </div>
                                    <!-- /.panel-body -->
                                </div>
                            </div>
                            <div class="tab-pane fade in" id="foodmenu">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-md-8">Food Menu Item</div>
                                            <div class="col-md-4">
                                                <div class="row pull-right" style="padding-right: 10px;">
                                                    <button class="btn btn-default"> <i class="fa fa-trash" aria-hidden="true"></i>
                                                        Delete</button>
                                                    <a href="{{route('admin-food-menu',$res_id)}}"><button class="btn btn-default" type="button"> <i class="fa fa-plus" aria-hidden="true"></i>
                                                            Add New</button></a>
                                                    <button class="btn btn-default"> <i class="fa fa-refresh" aria-hidden="true"></i>
                                                        Refresh List</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- /.panel-heading -->
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <div class="dataTable_wrapper">
                                                <table width="100%" class="table table-striped table-bordered table-hover" >
                                                    <thead>
                                                    <tr>
                                                        <th>Select</th>
                                                        <th>Item ID</th>
                                                        <th>Item Name</th>
                                                        <th>Food Category</th>
                                                        <th>Description</th>
                                                        <th>Price</th>
                                                        <th>Item Unavailable</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(!empty($data_menu))
                                                    @foreach($data_menu as $value_menu)
                                                    <tr class="odd gradeX">
                                                        <td><input type="checkbox"></td>
                                                        <td>ID-{{$value_menu['id']}}</td>
                                                        <td>{{$value_menu['menu_name']}}</td>
                                                        <td class="center">{{$value_menu['list_menu_category_cd']}}</td>
                                                        <td class="center">{{$value_menu['description']}}</td>
                                                        <td>
                                                            <?php $pr = '';
                                                                $count = count($value_menu['menu_price']);
                                                                 $i=1;
                                                            ?>
                                                            @foreach($value_menu['menu_price'] as $price)
                                                                    @foreach($size_cd as $key =>$value)
                                                                          @if($key ==  $price['menu_size_cd'])
                                                                             <?php  $size = $value; ?>
                                                                          @endif
                                                                    @endforeach

                                                                <?php
                                                                            if($i == $count){
                                                                                $pr .= $size.'-'.$price['price'].' USD';
                                                                            }else{
                                                                                $pr .= $size.'-'.$price['price'].' USD'." |";
                                                                            }


                                                                        ?>

                                                                <?php $i++;?>
                                                            @endforeach
                                                            {{$pr}}
                                                        </td>
                                                        <td><input type="checkbox"></td>
                                                        <td ><a href="#"><i class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i>
                                                            </a>&nbsp&nbsp<a href="#"><i class="fa fa-trash fa-2x" aria-hidden="true"></i></a>  </td>
                                                    </tr>
                                                    @endforeach
                                                    @endif



                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- /.table-responsive -->

                                    </div>
                                    <!-- /.panel-body -->
                                </div>
                            </div>
                            <div class="tab-pane fade in" id="user">
                                User
                            </div>
                            <div class="tab-pane fade in" id="order">
                                Order
                            </div>
                        </div>



                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
            @else
            <font color="red">Merchant not activate yet</font>
        @endif
    </div>
    @include('admin.include.popup-upload-banner')
    @include('admin.include.popup-loading')
@stop
@section('style')
    {!! Html::style('vendors/bootstrap-timepicker/css/bootstrap-timepicker.css') !!}
@stop
@section('script')
    {!! Html::script('vendors/bootstrap-timepicker/js/bootstrap-timepicker.js') !!}
    {!! Html::script('js/upload-image.js') !!}
    <script>
        $(document).ready(function(){

            $('.timepicker1').timepicker();

            $('.timepicker2').timepicker();


            $('.start-date').datetimepicker({
                format: 'YYYY-MM-DD',
            });
            $('.end-date').datetimepicker({
                format: 'YYYY-MM-DD',
            });


            clearAllText();
        });

        $('#cuisine').select2();
        $('#addrow-com').click(function(){
            $('#commis-table tbody').append('<tr> <td>Min Sell Amount</td><td>Max Sell Amount</td><td>Percentage</td>' +
                    '<td>Fix Amount</td><td>Min Commission Amount</td><td>Max Commission Amount</td></tr></thead><tbody><tr><td><div class="input-group">' +
                    '<input type="text" name="min_sell_amt[]" class="form-control"><span class="input-group-addon">$</span></div></td><td><div class="input-group">' +
                    '<input type="text" name="max_sell_amt[]" class="form-control"><span class="input-group-addon">$</span></div></td>' +
                    '<td><div class="input-group"><input type="text" name="commis_percentage[]" class="form-control"> <span class="input-group-addon">%</span></div>' +
                    '</td> <td><div class="input-group"><input type="text" name="commis_fix_amt[]" class="form-control"> <span class="input-group-addon">$</span></div> ' +
                    '</td><td><div class="input-group"> <input type="text" name="min_commission_amt[]" class="form-control">  <span class="input-group-addon">$</span></div>' +
                    '</td><td> <div class="input-group"><input type="text" name="max_commission_amt[]" class="form-control"><span class="input-group-addon">$</span></div>' +
                    '</td></tr>'

            );
            $('.start-date').datetimepicker({
                format: 'YYYY-MM-DD',
            });
            $('.end-date').datetimepicker({
                format: 'YYYY-MM-DD',
            });
        });
        var add = 1;
        $("#add-work").click(function(){

            $("#add-work-hour").append('<div class="work-hour" > <div class="btn-remove-row"><button class="btn btn-danger btn-circle" onclick="removeWork(this);" type="button"><i class="fa fa-minus" aria-hidden="true"></i></button> </div><table style="border-collapse: separate; border-spacing: 5px;">' +
                    '<tr><td> <table id="table-time"><tr><td> <div class="input-group bootstrap-timepicker timepicker" style="width: 200px !important;">' +
                    '<input  type="text" name="start_time[]" class="form-control input-small timepicker1" ><span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>' +
                    '</div></td>' +
                    '<td>To</td>' +
                    '<td> <div class="input-group bootstrap-timepicker timepicker"  style="width: 200px !important;">' +
                    '<input  type="text" name="end_time[]" class="form-control input-small timepicker2">' +
                    ' <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>' +
                    '</div></td></tr>' +
                    '<tr id="row-time"><td colspan="3"> ' +
                    '<ul class="days-week"> <input type="hidden" name="allcheck[]" class="allcheck"> <li><label><input type="checkbox" class="checkmon checkDay" name="check[]" style="display: none;"   value="Mon"><a onclick="dayofmon(this);">Mon</a></label></li>' +
                    ' <li><label><input type="checkbox" class="checktue checkDay" name="check[]"  style="display: none;" value="Tue"><a onclick="dayoftue(this);"> Tue</a></label></li>' +
                    ' <li><label><input type="checkbox" class="checkwed checkDay" name="check[]"  style="display: none;" value="Wed"><a onclick="dayofwed(this);"> Wed</a></label></li>' +
                    ' <li><label><input type="checkbox" class="checkthu checkDay" name="check[]"  style="display: none;"  value="Thu"><a onclick="dayofthu(this);"> Thu</a></label></li>' +
                    ' <li><label><input type="checkbox" class="checkfri checkDay" name="check[]"   style="display: none;"  value="Fri"><a onclick="dayoffri(this);"> Fri</a></label></li>' +
                    ' <li><label><input type="checkbox" class="checksat checkDay" name="check[]"  style="display: none;"  value="Sat"><a onclick="dayofsat(this);"> Sat</a></label></li>' +
                    ' <li><label><input type="checkbox" class="checksun checkDay" name="check[]"  style="display: none;"  value="Sun"><a onclick="dayofsun(this);"> Sun</a></label></li>' +

                    '</td></tr> </table></td></tr> </table> </div>');
            $('.timepicker1').timepicker();
            $('.timepicker2').timepicker();



            add++;

        });

        $("#update-add-logo").click(function(){

            $('#update-logo').trigger('click');
        });
        $("#update-add-banner").click(function(){

            $('#update-banner').trigger('click');
        });
        function removeWork(self){
            $(self).closest('.work-hour').remove();

        }

        function removeImage(self){
            $(self).closest('td').remove();

        }
        function removeUpdateLogo(self){
            $(self).closest('td').remove();
            $('#update-logo-picture').css('display','block');

        }
        function removeLogo(self){
            $(self).closest('td').remove();
            $('#logo-picture').css('display','block');

        }
        function removeBanner(self){
            $(self).closest('td').remove();
            $('#banner-picture').css('display','block');

        }
        function removeUpdateBanner(self){
            $(self).closest('td').remove();
            $('#update-banner-picture').css('display','block');

        }
        function removeVideo(self){
            $(self).closest('td').remove();
        }

        function dayofmon(self){
            var check = $(self).closest("td").find(".checkmon").is(":checked");
            if(check){
                $(self).closest("li").removeClass('active');
            }else{
                $(self).closest("li").addClass('active');
            }

        }
        $('body').on('click','.days-week input[type="checkbox"]',function(){
            var dayOfWeek = '';
            var parent = $(this).parents('.days-week');
            parent.find('.checkDay:checked').each(function(index,element){
                var checkBox = $(this);
                dayOfWeek += checkBox.val() + '|';

            });
            parent.find('.allcheck').val(dayOfWeek.slice(0,dayOfWeek.length -1));
        });

        $('body').on('change','.loop_social input[type="text"]',function(){
            var parent = $(this).parents('.loop_social');

            var urlPattern = new RegExp("(http|ftp|https)://([\w.,@?^=%&amp;:/~+#-]*[\w@?^=%&amp;/~+#-])?");
            var text = $(this).val();
            if(text!=''){
                if(urlPattern.test(text)){
                    parent.find('.pat_link').html('');
                    $(this).css('border-color','');
                }else{
                    parent.find('.pat_link').html('Invalid URL');
                    $(this).css('border-color','red');
                }
            }else{
                parent.find('.pat_link').html('');
                $(this).css('border-color','');
            }

        });

        function dayoftue(self){
            var check = $(self).closest("td").find(".checktue").is(":checked");
            if(check){
                $(self).closest("li").removeClass('active');
            }else{
                $(self).closest("li").addClass('active');
            }

        }
        function dayofwed(self){
            var check = $(self).closest("td").find(".checkwed").is(":checked");
            if(check){
                $(self).closest("li").removeClass('active');
            }else{
                $(self).closest("li").addClass('active');
            }

        }
        function dayofthu(self){
            var check = $(self).closest("td").find(".checkthu").is(":checked");
            if(check){
                $(self).closest("li").removeClass('active');
            }else{
                $(self).closest("li").addClass('active');
            }

        }
        function dayoffri(self){
            var check = $(self).closest("td").find(".checkfri").is(":checked");
            if(check){
                $(self).closest("li").removeClass('active');
            }else{
                $(self).closest("li").addClass('active');
            }

        }
        function dayofsat(self){
            var check = $(self).closest("td").find(".checksat").is(":checked");
            if(check){
                $(self).closest("li").removeClass('active');
            }else{
                $(self).closest("li").addClass('active');
            }

        }
        function dayofsun(self){
            var check = $(self).closest("td").find(".checksun").is(":checked");
            if(check){
                $(self).closest("li").removeClass('active');
            }else{
                $(self).closest("li").addClass('active');
            }

        }


        $("#select-location").click(function(){
            var lat = $("#lat").val();
            var lng = $("#lng").val();
            initialize(lat,lng);
            google.maps.event.addDomListener(window, 'load', initialize);
        });

        function initialize(lat,lng) {
            if(lat == '' && lng == ''){
                var latitude = 11.5500;
                var longitude = 104.9167;
            }else{
                var latitude = lat;
                var longitude = lng;
            }

            var zoom = 15;

            var LatLng = new google.maps.LatLng(latitude, longitude);
            document.getElementById('lat').value = latitude;
            document.getElementById('lng').value = longitude;
            var mapProp = {
                center: LatLng,
                zoom:12,
                mapTypeId:google.maps.MapTypeId.ROADMAP
            };
            var map=new google.maps.Map(document.getElementById("map-canvas"), mapProp);
            var marker = new google.maps.Marker({
                position: LatLng,
                map: map,
                title: 'Drag Me!',
                draggable: true
            });

            var searchbox = new google.maps.places.SearchBox(document.getElementById('searchmap'));
            google.maps.event.addListener(searchbox,'places_changed',function(){
                var places = searchbox.getPlaces();
                var bounds = new google.maps.LatLngBounds();
                var i,place;
                for(i=0;place=places[i];i++){
                    bounds.extend(place.geometry.location);
                    marker.setPosition(place.geometry.location);
                    document.getElementById('lat').value = place.geometry.location.lat();
                    document.getElementById('lng').value = place.geometry.location.lng();
                }
                map.fitBounds(bounds);
                map.setZoom(15);
            });
            google.maps.event.addListener(marker, 'dragend', function(event) {

                document.getElementById('lat').value = event.latLng.lat();
                document.getElementById('lng').value = event.latLng.lng();



            });
            google.maps.event.addListener(marker, 'places_changed', function(event) {

                document.getElementById('lat').value = event.latLng.lat();
                document.getElementById('lng').value = event.latLng.lng();



            });
        }


    </script>
    {!! Html::script('js/gmap.js') !!}
@stop