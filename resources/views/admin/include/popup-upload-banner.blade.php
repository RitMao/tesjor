{!! Form::open(array('files'=>true,'name' => 'frmJcrop','id'=>'frmJcrop')) !!}
<div id="modalJcrop" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Upload Image</h4>
            </div>
            <div class="modal-body">
                <div class="thumbnail my-thumbnail" style="padding: 30px; margin: auto;">
                    <img src="" id='view'>
                    <input type="hidden" id="type" name="type"/>
                    <input type="hidden" id="path" name="path" />
                    <input type="hidden" id="x" name="x" />
                    <input type="hidden" id="y" name="y" />
                    <input type="hidden" id="w" name="w" />
                    <input type="hidden" id="h" name="h" />
                    <input type="hidden" id="title" name="title">

                    {{--<div class="row" style="margin-top: 5px;">--}}
                        {{--<div class="col-md-12">--}}
                            {{--<div class="form-group">--}}
                                {{--<label>Title:</label>--}}
                                {{--{{Form::text('title',null,array('class' => 'form-control'))}}--}}

                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="row" style="margin-top: 5px;">--}}
                        {{--<div class="col-md-12">--}}
                            {{--<div class="form-group">--}}
                                {{--<label>Description:</label>--}}
                                {{--{{ Form::textarea('desc', null, ['cols' => '70','rows' => '7', 'class' => 'form-control']) }}--}}

                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="save-temp">Submit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
{!! Form::close() !!}