<div data-backdrop="static" class="modal modal-loading" id="modal-loading">
    <div class="modal-dialog">
        <div class="modal-content loading-content">
            <div class="modal-body">
                <img id="img-loader" src="{{asset('images/loading.gif')}}">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->