<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Tesjor</title>
    {!! Html::style('vendors/bootstrap/dist/css/bootstrap.min.css') !!}
    {!! Html::style('vendors/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') !!}
    {!! Html::style('components/css/admin.css') !!}
    {!! Html::style('components/font-awesome/css/font-awesome.min.css') !!}
    {!! Html::style('components/metisMenu/dist/metisMenu.min.css') !!}
    {!! Html::style('components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css') !!}
    {!! Html::style('components/datatables-responsive/css/dataTables.responsive.css') !!}
    {!! Html::style('components/font-awesome/css/font-awesome.min.css') !!}
    {!! Html::style('vendors/select2/dist/css/select2.min.css') !!}
    {!! Html::style('vendors/Jcrop/css/Jcrop.min.css')  !!}
<!-- AngularJS -->
  <link rel="stylesheet" href="{!!asset('css/app.css')!!}">
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC8ivLfYZg4MrUH65J6LYr11wjEOywyxDA&address=cambodia&libraries=places"
            type="text/javascript">
    </script>
    @yield('style')

</head>

<body>

<div id="wrapper">
@yield('content')

</div>
<!-- /#wrapper -->


{!! Html::script('vendors/jquery/dist/jquery.min.js') !!}
{!! Html::script('vendors/moment/min/moment.min.js') !!}
{!! Html::script('vendors/bootstrap/dist/js/bootstrap.min.js') !!}
{!! Html::script('vendors/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') !!}
{!! Html::script('components/metisMenu/dist/metisMenu.min.js') !!}
        <!-- DataTables JavaScript -->

{!! Html::script('components/datatables/media/js/jquery.dataTables.min.js') !!}
{!! Html::script('components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js') !!}
{!! Html::script('components/datatables-responsive/js/dataTables.responsive.js') !!}
{!! Html::script('vendors/select2/dist/js/select2.min.js') !!}
{!! Html::script('vendors/Jcrop/js/Jcrop.min.js')  !!}
{!! Html::script('vendors/jquery-validation/dist/jquery.validate.min.js') !!}
{!! Html::script('components/js/admin.js') !!}
<script type="text/javascript">
    var baseUrl = "{{ URL::to('/') }}";
</script>
@yield('script')

</body>

</html>
