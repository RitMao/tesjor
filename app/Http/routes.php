<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/' , [ 'as' =>'admin-login' , 'uses' => 'AdminController@adminLogin']);
Route::get('logout', [ 'as' => 'logout' , 'uses' => 'AdminController@adminLogout']);
Route::post('post-admin-login', ['as' => 'post-admin-login', 'uses' => 'AdminController@postAdminLogin']);
Route::get('activateaccount',['as' => 'activate-account', 'uses' => 'AdminController@ActivateAccount']);
Route::group(['middleware' => 'admin'],function() {

// ==========Restaurant Manage
Route::get('administrator/restaurant-list' , [ 'as' => 'admin-restaurant-list' , 'uses' => 'RestaurantController@restaurantList']);
Route::get('administrator/restaurant-profile', ['as' => 'admin-restaurant-profile', 'uses' => 'RestaurantController@restaurantProfile']);
Route::get('administrator/update-restaurant/{id}', ['as' => 'update-restaurant-list', 'uses' => 'RestaurantController@updateRestaurantList']);
Route::post('administrator/add-restaurant-profile', ['as' => 'post-admin-restaurant-profile', 'uses' => 'AdminController@postRestaurantProfile']);
   
   
});
