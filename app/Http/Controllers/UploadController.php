<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Config;
use DateTime;
use Cookie;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Input;
use Validator;
use Image;
use File;
use CurlFile;
class UploadController extends BaseController
{
    function uploadBannerTemp(Request $request){
        $input = array('banner' => $request->file('banner'));

        $rules = array(
            'banner' => 'required|image'
        );
        $file = $request->file('banner');
        $validator = Validator::make($input, $rules);

        if (!$validator->fails()){
            $destinationPath = 'images/temp/';
            $filename = str_random(8).'.'.$file->getClientOriginalExtension();

            $width = Image::make($request->file('banner'))->width();
            $height = Image::make($request->file('banner'))->height();

            Image::make($request->file('banner'))
                //->resize(1400, ($max_width * $height)/$width)
                ->save($destinationPath.$filename);
            //return $filename;
            echo json_encode(
                array("src" => $destinationPath.$filename,
                    "width" => $width,
                    "height" => $height));
        }



    }
    function uploadLogoTemp(Request $request){
        $input = array('logo' => $request->file('logo'));

        $rules = array(
            'logo' => 'required|image'
        );
        $file = $request->file('logo');
        $validator = Validator::make($input, $rules);

        if (!$validator->fails()){
            $destinationPath = 'images/temp/';
            $filename = str_random(8).'.'.$file->getClientOriginalExtension();

            $width = Image::make($request->file('logo'))->width();
            $height = Image::make($request->file('logo'))->height();

            Image::make($request->file('logo'))
                //->resize(1400, ($max_width * $height)/$width)
                ->save($destinationPath.$filename);
            //return $filename;
            echo json_encode(
                array("src" => $destinationPath.$filename,
                    "width" => $width,
                    "height" => $height));
        }



    }
    public function uploadCropBanner(Request $request){

        $x = $request->input('x');
        $y = $request->input('y');
        $w = $request->input('w');
        $h = $request->input('h');
        $src = $request->input('path');
        $img = Image::make($src);
        $mime = $img->mime();
        if ($mime == 'image/jpeg') {
            $extension = 'jpg';

        } elseif ($mime == 'image/png') {

            $extension = 'png';

        }

        $filename = str_random(16);
        $file_path = 'images/temp/' . $filename . '.' . $extension;

        $img->crop(intval($w), intval($h), intval($x), intval($y))->save($file_path);

        //delete file in temp
        if (File::exists($src)) {
            File::delete($src);
        }

        $final_src = public_path() . '/' . $file_path;


        $file = new \Symfony\Component\HttpFoundation\File\File($final_src);
        $token = 'Bearer '.$_COOKIE['token'];
        $user_session_id = $_COOKIE['session_user_id'];

        $header = array(
            "Content-Type" => "multipart/form-data",
            'Token' => $token,
            'Uses-Id' =>  $user_session_id
        );
        $data = array(
            'media' => new CURLFile($final_src, $file->getMimeType(), $filename),
            'photo_type_cd' => "PLG"
        );
        $status = $this->post("v1/media/photos/RTR/PLG",$data, $header);

        if ($status["headers"]["http_code"] == 200){
            $link = $status["responseText"];
            $url = parse_url($status["responseText"][0]);
            $cut_url = $url['path'];
            $ex_url = explode('/',$cut_url);
            $slash_url = $ex_url[3];
            $filename = explode('_',$slash_url);
            unlink($final_src);
            return json_encode(array('img_link' => $link,'filename' => $filename));
        }
        return Redirect::back()->withInput();
    }
    function uploadPhotos(Request $request){
        $file = $request->file('photos');
        $count = count($file);
      //  return $count;
        $token = 'Bearer '.$_COOKIE['token'];
        $user_session_id = $_COOKIE['session_user_id'];

        $header = array(
            "Content-Type" => "multipart/form-data",
            'Token' => $token,
            'Uses-Id' =>  $user_session_id
        );
        $arr_respon = array();
        for($i = 0; $i < $count; $i++){
            $files = $file[$i];
            $data = array(
                'media' => new CURLFile($files->getPathname(), $files->getMimeType(), $files->getClientOriginalName()),
                'photo_type_cd' => 'PGL'
            );
            $status = $this->post("v1/media/photos/RTR/PGL",$data, $header);

            $id =  $status['responseText'][0]['id'];
            $link =  $status['responseText'][0]['full_url'];
            $photo_type_cd = $status['responseText'][0]['photo_type_cd'];
            $sector_cd = $status['responseText'][0]['sector_cd'];
            $url = parse_url($link);
            $cut_url = $url['path'];
            $ex_url = explode('/',$cut_url);
            $slash_url = $ex_url[3];
            $filename = explode('_',$slash_url);


            $data['alldata']['id'] = $id;
            $data['alldata']['img_link'] = $link;
            $data['alldata']['photo_type'] = $photo_type_cd;
            $data['alldata']['filename'] = $filename;
            array_push($arr_respon, $data['alldata']);
        }
        return $arr_respon;
    }
    function UploadLogo(Request $request){
        $file = $request->file('logo');

        $token = 'Bearer '.$_COOKIE['token'];
        $user_session_id = $_COOKIE['session_user_id'];

        $header = array(
            "Content-Type" => "multipart/form-data",
            'Token' => $token,
            'Uses-Id' =>  $user_session_id
        );


        $data = array(
            'media' => new CURLFile($file->getPathname(), $file->getMimeType(), $file->getClientOriginalName()),
            'photo_type_cd' => 'PLG'
        );
        $result = $this->post("v1/media/photos/RTR/PLG",$data, $header);
       // return $status;
        if($result['headers']['http_code'] == 400){
            $status = false;
            $message = $result['responseText']['message'];
            return json_encode(array('status' => $status,'message' => $message));
        }else if($result['headers']['http_code'] == 200){
            $status = true;
            $id =  $result['responseText'][0]['id'];
            $link =  $result['responseText'][0]['full_url'];
            $photo_type_cd = $result['responseText'][0]['photo_type_cd'];
            $sector_cd = $result['responseText'][0]['sector_cd'];
            $url = parse_url($link);
            $cut_url = $url['path'];
            $ex_url = explode('/',$cut_url);
            $slash_url = $ex_url[3];
            $filename = explode('_',$slash_url);
            return json_encode(array('status' => $status,'img_link' => $link,'id' => $id,'photo_type' => $photo_type_cd,'sector_cd' => $sector_cd,'filename' => $filename));
        }





    }
    function UploadBanner(Request $request){
        $file = $request->file('banner');

        $token = 'Bearer '.$_COOKIE['token'];
        $user_session_id = $_COOKIE['session_user_id'];

        $header = array(
            "Content-Type" => "multipart/form-data",
            'Token' => $token,
            'Uses-Id' =>  $user_session_id
        );


        $data = array(
            'media' => new CURLFile($file->getPathname(), $file->getMimeType(), $file->getClientOriginalName()),
            'photo_type_cd' => 'PLG'
        );
        $result = $this->post("v1/media/photos/RTR/PBN",$data, $header);

        if($result['headers']['http_code'] == 400){
            $status = false;
            $message = $result['responseText']['message'];
            return json_encode(array('status' => $status,'message' => $message));
        }else if($result['headers']['http_code'] == 200){
            $status = true;
            $id =  $result['responseText'][0]['id'];
            $link =  $result['responseText'][0]['full_url'];
            $photo_type_cd = $result['responseText'][0]['photo_type_cd'];
            $sector_cd = $result['responseText'][0]['sector_cd'];
            $url = parse_url($link);
            $cut_url = $url['path'];
            $ex_url = explode('/',$cut_url);
            $slash_url = $ex_url[3];
            $filename = explode('_',$slash_url);
            return json_encode(array('status' => $status,'img_link' => $link,'id' => $id,'photo_type' => $photo_type_cd,'sector_cd' => $sector_cd,'filename' => $filename));
        }
    }
    function UploadPromotion(Request $request){
    $file = $request->file('promo_img');

    $token = 'Bearer '.$_COOKIE['token'];
    $user_session_id = $_COOKIE['session_user_id'];

    $header = array(
        "Content-Type" => "multipart/form-data",
        'Token' => $token,
        'Uses-Id' =>  $user_session_id
    );


    $data = array(
        'media' => new CURLFile($file->getPathname(), $file->getMimeType(), $file->getClientOriginalName()),
        'photo_type_cd' => 'PBP'
    );
    $result = $this->post("v1/media/photos/RTR/PBP",$data, $header);

    if($result['headers']['http_code'] == 400){
        $status = false;
        $message = $result['responseText']['message'];
        return json_encode(array('status' => $status,'message' => $message));
    }else if($result['headers']['http_code'] == 200){
        $status = true;
        $id =  $result['responseText'][0]['id'];
        $link =  $result['responseText'][0]['full_url'];
        $photo_type_cd = $result['responseText'][0]['photo_type_cd'];
        $sector_cd = $result['responseText'][0]['sector_cd'];
        $url = parse_url($link);
        $cut_url = $url['path'];
        $ex_url = explode('/',$cut_url);
        $slash_url = $ex_url[3];
        $filename = explode('_',$slash_url);
        return json_encode(array('status' => $status,'img_link' => $link,'id' => $id,'photo_type' => $photo_type_cd,'sector_cd' => $sector_cd,'filename' => $filename));
    }
}
    function UploadMenu(Request $request){
        $file = $request->file('menu_img');

        $token = 'Bearer '.$_COOKIE['token'];
        $user_session_id = $_COOKIE['session_user_id'];

        $header = array(
            "Content-Type" => "multipart/form-data",
            'Token' => $token,
            'Uses-Id' =>  $user_session_id
        );


        $data = array(
            'media' => new CURLFile($file->getPathname(), $file->getMimeType(), $file->getClientOriginalName()),
            'photo_type_cd' => 'PBM'
        );
        $result = $this->post("v1/media/photos/RTR/PBM",$data, $header);
       // return $result;
        if($result['headers']['http_code'] == 400){
            $status = false;
            $message = $result['responseText']['message'];
            return json_encode(array('status' => $status,'message' => $message));
        }else if($result['headers']['http_code'] == 200){
            $status = true;
            $id =  $result['responseText'][0]['id'];
            $link =  $result['responseText'][0]['full_url'];
            $photo_type_cd = $result['responseText'][0]['photo_type_cd'];
            $sector_cd = $result['responseText'][0]['sector_cd'];
            $url = parse_url($link);
            $cut_url = $url['path'];
            $ex_url = explode('/',$cut_url);
            $slash_url = $ex_url[3];
            $filename = explode('_',$slash_url);
            return json_encode(array('status' => $status,'img_link' => $link,'id' => $id,'photo_type' => $photo_type_cd,'sector_cd' => $sector_cd,'filename' => $filename));
        }
    }
    function uploadFoodImage(Request $request){
        $file = $request->file('food-img');

        $token = 'Bearer '.$_COOKIE['token'];
        $user_session_id = $_COOKIE['session_user_id'];

        $header = array(
            "Content-Type" => "multipart/form-data",
            'Token' => $token,
            'Uses-Id' =>  $user_session_id
        );


            $data = array(
                'media' => new CURLFile($file->getPathname(), $file->getMimeType(), $file->getClientOriginalName()),
                'title' => 'Merchant_food',
                'file_name' => '',
                'description' => '',
                'photo_type_cd' => 'PLG'
            );
            $status = $this->post("v1/media/photo",$data, $header);
            $url = parse_url($status["responseText"][0]);
            $cut_url = $url['path'];
            $ex_url = explode('/',$cut_url);
            $slash_url = $ex_url[3];
            $filename = explode('_',$slash_url);


        return json_encode(array('img_link' => $status["responseText"],'filename' => $filename));
    }
    function uploadPhotosDrag(Request $request){
        $file = $request->file('photos_drag');
        $count = count($file);
        //  return $count;
        $token = 'Bearer '.$_COOKIE['token'];
        $user_session_id = $_COOKIE['session_user_id'];

        $header = array(
            "Content-Type" => "multipart/form-data",
            'Token' => $token,
            'Uses-Id' =>  $user_session_id
        );
        $arr_respon = array();
        for($i = 0; $i < $count; $i++){
            $files = $file[$i];
            $data = array(
                'media' => new CURLFile($files->getPathname(), $files->getMimeType(), $files->getClientOriginalName()),
                'photo_type_cd' => 'PGL'
            );
            $status = $this->post("v1/media/photos/RTR/PGL",$data, $header);

            $id =  $status['responseText'][0]['id'];
            $link =  $status['responseText'][0]['full_url'];
            $photo_type_cd = $status['responseText'][0]['photo_type_cd'];
            $sector_cd = $status['responseText'][0]['sector_cd'];
            $url = parse_url($link);
            $cut_url = $url['path'];
            $ex_url = explode('/',$cut_url);
            $slash_url = $ex_url[3];
            $filename = explode('_',$slash_url);


            $data['alldata']['id'] = $id;
            $data['alldata']['img_link'] = $link;
            $data['alldata']['photo_type'] = $photo_type_cd;
            $data['alldata']['filename'] = $filename;
            array_push($arr_respon, $data['alldata']);
        }
        return $arr_respon;
    }
}
