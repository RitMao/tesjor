<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Config;
use Session;
use Response;

class MapController extends BaseController
{
    public function index($merchant_id){
   		$user_session_id = $this->createRandomVal(19);
        $client_id =  Config::get('webservice.client_id');
        $client_secret = Config::get('webservice.client_secret');
        $code = $client_id.':'.$client_secret;
        $auth = "Bearer ".sha1($code);
        $header = array(
            "Authorization" => $auth,
            "Uses-Id" =>  $user_session_id
        );
        $data = array("client_id" => $client_id);
        $result = $this->post('v1/auth/authorize',$data,$header);
        $result = $result['responseText'];


   		$token = 'Bearer '.$result['token'];
        $user_session_id = $user_session_id;

        $header = array(
            'Token' => $token,
            'Uses-Id' =>  $user_session_id
        );
    
        $map_result = $this->get('v1/dataservice/merchants/RTR/' . $merchant_id, $header);

    	return view('map.index')->with('data', $map_result['responseText']);
    }
}
