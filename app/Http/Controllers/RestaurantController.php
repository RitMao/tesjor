<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use Config;
use DateTime;
use Cookie;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Input;

class RestaurantController extends BaseController
{
    function restaurantList(){

        $token = 'Bearer '.$_COOKIE['token'];
        $user_session_id = $_COOKIE['session_user_id'];
        $header = array(
            'Token' => $token,
            'Uses-Id' =>  $user_session_id
        );
        $status = $this->getCodeStatus();
        $sta_cd = $status['item_code'];

        $result = $this->get('v1/dataservice/merchants/RTR',$header);
  
        if($result['headers']['http_code']==200){
            return view('admin.restaurant-list')->with('data',$result['responseText'])->with('status',$sta_cd);

        }elseif($result['headers']['http_code']==400){
            return view('admin.restaurant-list')->with('data','');
        }

    }
	function restaurantProfile(){
        $mer_code = $this->getCodeMerchant();
        $social = $this->getCodeSocial();
        $tran_cd = $this->getCodeTransaction();
        $cuisine_cd = $this->getCodeCuisine();
        $country = $this->getCountry();
        return view('admin.restaurant-profile')->with('country',$country)->with('social',$social)->with('merchant_cd',$mer_code)->with('tran_cd',$tran_cd)->with('cuisine_cd',$cuisine_cd);
    }

}
