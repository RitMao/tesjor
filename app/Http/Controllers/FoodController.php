<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use Config;
use DateTime;
use Cookie;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Input;
class FoodController extends BaseController{
    
    public function postFood(Request $request){ 

      

        $token = 'Bearer '.$this->getProperty('access_token');

        $user_session_id = $_COOKIE['session_user_id'];

        $header = array(
            'X-AUTH' => $token,
            'Uses-Id' =>  $user_session_id
        );
         $cuisine = $request->input('cuisine');
        $count_cuis = count($cuisine);
        $j=1;
        $text_cuisine = "";
        for($i=0;$i<$count_cuis;$i++){
            if($j==$count_cuis){
                $cui = $cuisine[$i];
            }else{
                $cui = $cuisine[$i]."|";
            }
                $text_cuisine .=$cui;
            $j++;
        }

        $meal = $request->input('meal');
        $count_meal = count($meal);
        $j=1;
        $text_meal = "";
        for($i=0;$i<$count_meal;$i++){
            if($j==$count_meal){
                $cui = $meal[$i];
            }else{
                $cui = $meal[$i]."|";
            }
                $text_meal .=$cui;
            $j++;
        }

        $code_addon = $request->input('code_addon');
        if(!empty($code_addon)) {
            $count_addon = count($code_addon);
            $price_addon = $request->input('price_addon');
            $addon_arr = array();
            for ($i = 0; $i < $count_addon; $i++) {
                $data = array(
                    'menu_addon_cd' => $code_addon[$i],
                    'price' => $price_addon[$i],
                    'default_currency_cd' => "USD",
                    'description' => "",
                );
                array_push($addon_arr, $data);
            }
        }else{
            $addon_arr = [];
        }

        $code_extra = $request->input('code_extra');

        $count_extra = count($code_extra);
        $price_extra = $request->input('price_extra');
        $extra_arr = array();
        for($i=0;$i<$count_extra;$i++){
            $data = array(
                'menu_extra_cd' => $code_extra[$i],
                'price'=> $price_extra[$i],
                'default_currency_cd'=>"USD",
                'description' =>"",
            );
            array_push($extra_arr,$data);
        }
      //  dd($extra_arr);

            $size_radio = $request->input('size_radio');
         if(!empty($size_radio)){
            $size_price = $request->input('size_price');
            //dd($size_price);
            $code_size = $request->input('code_size');

            $count_size = count($code_size);
            $size_arr = array();
            for($i=0;$i<$count_size;$i++){
                if($size_radio == $code_size[$i]){
                    $data = array(
                        'menu_size_cd' => $code_size[$i],
                        'price' => $size_price[$i],
                        'default_currency_cd' => 'USD',
                        'description' => "",
                        'is_default' => true
                    );
                }else{
                    $data = array(
                        'menu_size_cd' => $code_size[$i],
                        'price' => $size_price[$i],
                        'default_currency_cd' => 'USD',
                        'description' => "",
                        'is_default' => false
                    );
                }

                array_push($size_arr,$data);
            }
        }else{
            $size_arr= [];
        }
        $cook_array = array();
        if(!empty($request->input('cook_radio'))){
            $data =   array(
                   'cooking_preference_cd' => $request->input('cook_radio') ,
                   'description' => "",
                   'is_default' => true
               );
            array_push( $cook_array,$data);
        }else{
            $cook_array = [];
        }
       // dd($size_arr);
        $id = $request->input('res_id');
        $data = array(
        	 'menu_name' => $request->input('food_menu'),
        	 'ingredient'=>$request->input('ingredient'),
        	 'description'=> $request->input('desc'),
        	 'list_menu_category_cd'=>  $text_cuisine,
             'list_meal_time_cd'=> $text_meal,
             "status_cd" => "ACT",
        	 'menu_price' => $size_arr,
            'cooking_preference' => $cook_array,
            'menu_addon' => $addon_arr,
            'menu_extra' => $extra_arr,
		    'banner' => array(
                    'id' => $request->input('menu_id'),
                    'title' => $request->input('menu_title'),
                    'file_name'=> $request->input('menu_filename'),
                    'full_url'=> $request->input('menu_link'),
                    'description' => "",
            )
        ); 

           //dd($data);
	      $result = $this->post('v1/dataservice/merchants/RTR/'.$id.'/menus',$data,$header);
       // dd($result['responseText']['message']);
          if($result['headers']['http_code']==400){
             // return ('asa');
              return redirect()->back()->withInput()->with('error-message',$result['responseText']['message']);
          }elseif($result['headers']['http_code']==200){
              return redirect()->back()->with('success-message','insert data successful');
          }
    }


    
}
