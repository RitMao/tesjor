<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Config;
use DateTime;
use Cookie;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Input;
class AdminController extends BaseController{
    function adminLogin(){

        if(empty($_COOKIE['token'])){
          $this->returnTokenAuthorization();
        }

            return view('admin.login'); 
       
    }
    function postAdminLogin(Request $request){

        if(empty($_COOKIE['token'])){
            $this->returnTokenAuthorization();
            return Redirect::route('admin-login');
        }else{
            $token = 'Bearer '.$_COOKIE['token'];
            $user_session_id = $_COOKIE['session_user_id'];

            $email = $request->input('email');
            $password = $request->input('password');
           // $password = base64_encode(hash('sha256',$pass, true));
            $header = array(
                "Token" => $token,
                "Uses-Id" =>  $user_session_id
            );
            $data = array(
                "email" => $email,
                "password" => $password
            );
            $result = $this->post('v1/users/login/email_password',$data,$header);
            if($result['headers']['http_code'] == 200){
               $this->login($result['responseText']);
                return Redirect::route('admin-restaurant-list');
            }else if($result['headers']['http_code'] == 401){
                $this->returnRefreshToken();
                return Redirect::route('admin-login');
            }else if($result['headers']['http_code']==400){
                return Redirect::route('admin-login')->with('error-message',$result['responseText']['message']);
            }
        }
    }

    function adminLogout(){
        $this->logout();
        return Redirect::route('admin-login');
    }

}
