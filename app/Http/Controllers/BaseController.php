<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Config;
use Session;
use Response;
use token;
class BaseController extends Controller {

    private $content_type = 'application/json';

    /*
        @purpose: customize view path
        @string path: new path of view
        @author : admin
    */
    public function view($path)
    {
        $d_path = substr(get_class($this), 0, strrpos(get_class($this), '\\'));
        $d_path = lcfirst(str_replace("\\",".",$d_path));
        return view($d_path . '.Views.'. $path) ->with('app_template' , $this->getAppTemplate());
    }

    /*
        @purpose: return app template path
        @author : admin
    */
    public function getAppTemplate()
    {
        return array(
            'admin' => "app.Myapp.Templates.admin",
            'front' => "app.Myapp.Templates.front",
            'partials' =>  "app.Myapp.Templates.Partials."
        );
    }

    private function sanitizeUri($uri) {
        return str_replace('$', '/', $uri);
    }


    /*
        @purpose: get api url from config/webservice.php
        @author : admin
    */
    public function getServerAddress() {
        $address = Config::get('webservice.scheme') . '://' . Config::get('webservice.hostname') . '/';
        return $address;
    }
    /*
        @purpose: set header
        @author : admin
    */
    private function translateHeaders($header, $data = null) {

        if(!isset($header['Content-Type'])) $header['Content-Type'] = 'application/json';

        if (\Auth::check()) {
            $header['xauth'] = \Auth::getProperty('usr_access_token');
        }

        if($data){
            $header['Content-Length'] = strlen($data);
        }

//        if(Session::has('user')){
//            $header['X-AUTH'] = Session::get('user')['access_key'];
//        }

        if(!isset($header['X-Forwarded-For'])) $header['X-Forwarded-For'] = filter_var( $_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 );
        if(!isset($header['X-Real-IP'])) $header['X-Real-IP'] = filter_var( $_SERVER['SERVER_NAME'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 );
        if(!isset($header['Expect'])) $header['Expect'] = '';
        if(!isset($header['Origin'])) $header['Origin'] = 'Tesjor-Website';
        if(!isset($header['Accept-Encoding'])) $header['Accept-Encoding'] = 'gzip, deflate';
        if(!isset($header['User-Agent'])) $header['User-Agent'] = 'Tesjor';
       // $header['Authorization'] = "123456";
        $result = array();
        foreach ($header as $key => $value) {
            $result[] = $key . ': ' . $value;
        }

        return $result;
    }
    /*
        @purpose: generate response format
        @author : admin
    */
    private function response($res, $resp) {
        $encodedResp = $resp;
        $headers = curl_getinfo($res);
        try {
            if(strpos($headers['content_type'], 'json') !== false) {
                $encodedResp = json_decode($encodedResp, true);
            } else {
                $encodedResp = $encodedResp;
            }
        }
        catch(\Exception $e) {
        }
        $result = array('headers' => $headers, 'responseText' => $encodedResp);
        curl_close($res);

        return $result;
    }
    /*
        @purpose: request api method GET
        @string uri: api end point
        @array header: request header type
        @author : admin
    */
    public function get($uri, $headers) {
        $uri = $this->getServerAddress() . $this->sanitizeUri($uri);
        $curl = curl_init();
        $options = array(
            CURLOPT_ENCODING => "gzip",
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $uri,
            CURLOPT_USERAGENT => 'Thmey9',
            CURLOPT_HTTPHEADER => $this->translateHeaders($headers));
        /* for verify ssl */
        $options[CURLOPT_SSL_VERIFYHOST] = 0;
        $options[CURLOPT_SSL_VERIFYPEER] = 0;

        curl_setopt_array($curl, $options);
        $resp = curl_exec($curl);
        return $this->response($curl, $resp);
    }

    /*
        @purpose: request api method POST
        @string uri: api end point
        @array data: data need to be post
        @array header: request header type
        @author : admin
    */
    public function post($uri, $data, $headers) {

        $uri = $this->getServerAddress() . $this->sanitizeUri($uri);
        if(isset($headers['Content-Type']) && ($headers['Content-Type']  == 'multipart/form-data')){
            $data = $data;

        } else {
            $data = json_encode($data);
        }
        /* Get cURL resource */
        $curl = curl_init();
        $options = array(CURLOPT_ENCODING => "gzip",CURLOPT_CUSTOMREQUEST => 'POST', CURLOPT_HTTPPROXYTUNNEL=> 1, CURLOPT_FOLLOWLOCATION=> 1, CURLOPT_RETURNTRANSFER => 1, CURLOPT_URL => $uri, CURLOPT_USERAGENT => 'Savada', CURLOPT_POST => 1, CURLOPT_POSTFIELDS => $data, CURLOPT_HTTPHEADER => $this->translateHeaders($headers), CURLINFO_HEADER_OUT => true);

        if (strpos($uri, 'api-dev') == true){
            /* without ssh */
            $options[CURLOPT_SSL_VERIFYHOST] = 0;
            $options[CURLOPT_SSL_VERIFYPEER] = 0;
        }

        else{
            /* with ssh */
            $options[CURLOPT_SSL_VERIFYHOST] = 2;
            $options[CURLOPT_SSL_VERIFYPEER] = 2;
        }

        curl_setopt_array($curl, $options);

        /* Send the request & save response to $resp */

        $resp = curl_exec($curl);
        return $this->response($curl, $resp);
    }

    /*
        @purpose: request api method PUT (update)
        @string uri: api end point
        @array data: data need to be update
        @array header: request header type
        @author : admin
    */
    public function put($uri, $data, $headers) {
        $uri = $this->getServerAddress() . $this->sanitizeUri($uri);
        $headers['X-HTTP-Method-Override'] = 'PUT';

        $curl = curl_init();

        /* Set some options - we are passing in a useragent too here */
        curl_setopt_array($curl, array(CURLOPT_ENCODING => "gzip",CURLOPT_RETURNTRANSFER => 1, CURLOPT_URL => $uri, CURLOPT_USERAGENT => 'Savada', CURLOPT_POSTFIELDS => json_encode($data), CURLOPT_HTTPHEADER => $this->translateHeaders($headers), CURLOPT_CUSTOMREQUEST => 'PUT'));

        /* Send the request & save response to $resp */
        $resp = curl_exec($curl);
        return $this->response($curl, $resp);
    }

    /*
        @purpose: request api method DELETE
        @string uri: api end point
        @array data: data need to be send
        @array header: request header type
        @author : admin
    */
    public function delete($uri, $data, $headers) {
        $uri = $this->getServerAddress() . $this->sanitizeUri($uri);
        $headers['X-HTTP-Method-Override'] = 'DELETE';

        /*Get cURL resource*/
        $curl = curl_init();

        /* Set some options - we are passing in a useragent too here*/
        curl_setopt_array($curl, array(CURLOPT_ENCODING => "gzip",CURLOPT_RETURNTRANSFER => 1, CURLOPT_URL => $uri, CURLOPT_USERAGENT => 'Savada', CURLOPT_POSTFIELDS => json_encode($data), CURLOPT_HTTPHEADER => $this->translateHeaders($headers), CURLOPT_CUSTOMREQUEST => 'DELETE'));

        /* Send the request & save response to $resp*/
        $resp = curl_exec($curl);
        return $this->response($curl, $resp);
    }

    /*
        @purpose: login (create session of user data)
        @array data: user object
        @author : admin
    */
    public function login($user_data){
        Session::put('user', $user_data);
        Session::put('access_key', @Session::get('user')['access_key']);
    }

    /*
        @purpose: check is user login?
        @author : admin
    */
    public function isLogin(){
        $user = $this->getUser();
        if ( is_array($user) && isset($user['access_key']) ){
            return true;
        } else {
            return false;
        }
    }

    /*
        @purpose: get property of user object
        @author : admin
    */
    public function getProperty($item){
        $user = $this->getUser();
        if(isset($user)){
            return $user[$item];
        }
        return false;
    }
    /*
        @purpose: get user object
        @author : admin
    */
    public function getUser(){
        $user = Session::get('user');
        return $user;
    }
    /*
        @purpose: logout
        @author : admin
    */
    public function logout(){
        Session::forget('id');
        Session::flush();
    }
    /*
       @purpose: get Token from authorization
       @author: Ben
    */
    public function returnTokenAuthorization(){
        $user_session_id = $this->createRandomVal(19);
        $client_id =  Config::get('webservice.client_id');
        $client_secret = Config::get('webservice.client_secret');
        $code = $client_id.':'.$client_secret;
        $auth = "Bearer ".sha1($code);
        $header = array(
            "Authorization" => $auth,
            "Uses-Id" =>  $user_session_id
        );
        $data = array("client_id" => $client_id);
        $result = $this->post('v1/auth/authorize',$data,$header);
        $result = $result['responseText'];
       // return $result;
        setcookie('session_user_id',$user_session_id);
        setcookie('token',$result['token']);
        setcookie('refresh_token',$result['refresh_token']);

    }

    public function getTokenNOCookie(){
        $user_session_id = $this->createRandomVal(19);
        $client_id =  Config::get('webservice.client_id');
        $client_secret = Config::get('webservice.client_secret');
        $code = $client_id.':'.$client_secret;
        $auth = "Bearer ".sha1($code);
        $header = array(
            "Authorization" => $auth,
            "Uses-Id" =>  $user_session_id
        );
        $data = array("client_id" => $client_id);
        $result = $this->post('v1/auth/authorize',$data,$header);
        $result['session'] = $user_session_id;
        return $result;
    }
       /*
           @purpose: get token when token expired by use refresh token
           @author: Ben
        */
    public function returnRefreshToken(){
        $refresh_token = "Bearer ".$_COOKIE['refresh_token'];
        $user_session_id = $_COOKIE['session_user_id'];
        $header = array(
            'Refresh-Token' => 'Bearer 528c4f0a1c4c498b89ba55044180224a',
            'Uses-Id' =>  'dEujbbuEiEkioDfnBzeG'
        );
        $result = $this->post('v1/auth/refresh-token',array(),$header);
        $result = $result['responseText'];
        setcookie('session_user_id', $user_session_id);
        setcookie('token', $result['token']);
        setcookie('refresh_token', $result['refresh_token']);
    }
    /*
      @purpose: Generate session user id
      @author: Ben
    */
    public function createRandomVal($val){
        $chars="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789,-";
        srand((double)microtime()*1000000);
        $i = 0;
        $pass = '' ;
        while ($i<=$val)
        {
            $num  = rand() % 33;
            $tmp  = substr($chars, $num, 1);
            $pass = $pass . $tmp;
            $i++;
        }
        return $pass;
    }
    /*
     * @purpose: Get code from table code for social_cd
     * @author: Ben
     */

    public function getCodeSocial(){
        $token = 'Bearer '.$_COOKIE['token'];
        $user_session_id = $_COOKIE['session_user_id'];

        $header = array(
            'Token' => $token,
            'Uses-Id' =>  $user_session_id
        );
        $result = $this->get('v1/utils/code-table',$header);
        $result = $result['responseText'];
         $arr = array();
        foreach($result as $social){
            if($social['reference_column_name'] == 'social_page_cd'){
                array_push($arr,$social['item_code']);
            }
        }
        return $arr;
    }
    public function getCodeStatus(){
        $token = 'Bearer '.$_COOKIE['token'];
        $user_session_id = $_COOKIE['session_user_id'];

        $header = array(
            'Token' => $token,
            'Uses-Id' =>  $user_session_id
        );
        $result = $this->get('v1/utils/code-table',$header);
        $result = $result['responseText'][1];
        return $result;
    }
    public function getCodeMerchant(){
        $token = 'Bearer '.$_COOKIE['token'];
        $user_session_id = $_COOKIE['session_user_id'];

        $header = array(
            'Token' => $token,
            'Uses-Id' =>  $user_session_id
        );
        $result = $this->get('v1/utils/code-table',$header);
        $result = $result['responseText'];
        $merchant_code = array();
        foreach($result as $merchant){
            if($merchant['reference_column_name'] == 'merchant_class_cd'){
                foreach($merchant['item_code'] as  $value){
                    if($value['is_active']==true){
                        $merchant_code[$value['item_code']] = $value['item_value'];

                    }
                }
            }
        }
        return $merchant_code;
    }
    public function getCodeTransaction(){
        $token = 'Bearer '.$_COOKIE['token'];
        $user_session_id = $_COOKIE['session_user_id'];

        $header = array(
            'Token' => $token,
            'Uses-Id' =>  $user_session_id
        );
        $result = $this->get('v1/utils/code-table',$header);
        $result = $result['responseText'];
        $arr = array();
        foreach($result as $value){
            if($value['reference_column_name'] == 'transaction_type_cd'){
                array_push($arr,$value['item_code']);
            }
        }
        return $arr;
    }
    public function getCodeCuisine(){
        $token = 'Bearer '.$_COOKIE['token'];
        $user_session_id = $_COOKIE['session_user_id'];

        $header = array(
            'Token' => $token,
            'Uses-Id' =>  $user_session_id
        );
        $result = $this->get('v1/utils/code-table',$header);
        $result = $result['responseText'];
        $arr = array();
        $cui_code = array();
        foreach($result as $value){
            if($value['reference_column_name'] == 'menu_category_cd'){
                foreach($value['item_code'] as $key => $value){
                    if($value['is_active']==true){
                        $cui_code[$value['item_code']] = $value['item_value'];

                    }
                }
            }
        }

        return $cui_code;
    }
    public function getCodeSize(){
         $token = 'Bearer '.$_COOKIE['token'];
        $user_session_id = $_COOKIE['session_user_id'];
        $header = array(
            'Token' => $token,
            'Uses-Id' =>  $user_session_id
        );
        $result = $this->get('v1/utils/code-table',$header);
        $result = $result['responseText'];
        $arr = array();
        $size_code = array();
        foreach($result as $value){
            if($value['reference_column_name'] == 'menu_size_cd'){
                foreach($value['item_code'] as $key => $value){
                    if($value['is_active']==true){
                        $size_code[$value['item_code']] = $value['item_value'];

                    }
                }
            }
        }
        return $size_code;
    }
    public function getCodeMeal(){
        $token = 'Bearer '.$_COOKIE['token'];
        $user_session_id = $_COOKIE['session_user_id'];

        $header = array(
            'Token' => $token,
            'Uses-Id' =>  $user_session_id
        );
        $result = $this->get('v1/utils/code-table',$header);
        $result = $result['responseText'];
        $arr = array();
        $meal_code = array();
        foreach($result as $value){
            if($value['reference_column_name'] == 'meal_time_cd'){
                foreach($value['item_code'] as $key => $value){
                    if($value['is_active']==true){
                        $meal_code[$value['item_code']] = $value['item_value'];

                    }
                }
            }
        }
        return $meal_code;

    }
    public function getCodeadOn(){
        $token = 'Bearer '.$_COOKIE['token'];
        $user_session_id = $_COOKIE['session_user_id'];

        $header = array(
            'Token' => $token,
            'Uses-Id' =>  $user_session_id
        );
        $result = $this->get('v1/utils/code-table',$header);
        $result = $result['responseText'];
        $arr = array();
        $addon_code = array();
        foreach($result as $value){
            if($value['reference_column_name'] == 'menu_addon_cd'){
                foreach($value['item_code'] as $key => $value){
                    if($value['is_active']==true){
                        $addon_code[$value['item_code']] = $value['item_value'];

                    }
                }
            }
        }
        return $addon_code;

    }

    public function getCodePreference(){
        $token = 'Bearer '.$_COOKIE['token'];
        $user_session_id = $_COOKIE['session_user_id'];

        $header = array(
            'Token' => $token,
            'Uses-Id' =>  $user_session_id
        );
        $result = $this->get('v1/utils/code-table',$header);
        $result = $result['responseText'];
        $arr = array();
        $preference_code = array();
        foreach($result as $value){
            if($value['reference_column_name'] == 'cooking_preference_cd'){
                foreach($value['item_code'] as $key => $value){
                    if($value['is_active']==true){
                        $preference_code[$value['item_code']] = $value['item_value'];

                    }
                }
            }
        }
        return $preference_code;

    }

    public function getCodeExtra(){
        $token = 'Bearer '.$_COOKIE['token'];
        $user_session_id = $_COOKIE['session_user_id'];

        $header = array(
            'Token' => $token,
            'Uses-Id' =>  $user_session_id
        );
        $result = $this->get('v1/utils/code-table',$header);
        $result = $result['responseText'];
        $arr = array();
        $extra_code = array();
        foreach($result as $value){
            if($value['reference_column_name'] == 'menu_extra_cd'){
                foreach($value['item_code'] as $key => $value){
                    if($value['is_active']==true){
                        $extra_code[$value['item_code']] = $value['item_value'];

                    }
                }
            }
        }
        return $extra_code;

    }
    
    
    public function getCountry(){
        $token = 'Bearer '.$_COOKIE['token'];
        $user_session_id = $_COOKIE['session_user_id'];

        $header = array(
            'Token' => $token,
            'Uses-Id' =>  $user_session_id
        );
        $result = $this->get('v1/utils/countrys',$header);
        $result = $result['responseText'];

        return $result;

    }
    public function getCodePromotion(){
        $token = 'Bearer '.$_COOKIE['token'];
        $user_session_id = $_COOKIE['session_user_id'];

        $header = array(
            'Token' => $token,
            'Uses-Id' =>  $user_session_id
        );
        $result = $this->get('v1/utils/code-table',$header);
        $result = $result['responseText'];
        $pro_code = array();
        foreach($result as $value){
            if($value['reference_column_name'] == 'promotion_type_cd'){
                foreach($value['item_code'] as $key => $value){
                    if($value['is_active']==true){
                        $pro_code[$value['item_code']] = $value['item_value'];

                    }
                }
            }
        }
        return $pro_code;
    }
    public function getCodeMinCoupon(){
        $token = 'Bearer '.$_COOKIE['token'];
        $user_session_id = $_COOKIE['session_user_id'];

        $header = array(
            'Token' => $token,
            'Uses-Id' =>  $user_session_id
        );
        $result = $this->get('v1/utils/code-table',$header);
        $result = $result['responseText'];
        $coupon_code = array();
        foreach($result as $value){
            if($value['reference_column_name'] == 'default_setting_cd'){
                foreach($value['item_code'] as $key => $value){
                    if($value['item_code']=='MPA'){
                        $coupon_code = $value['item_setting_value'];

                    }
                }
            }
        }
        return $coupon_code;
    }
    public function getCodePeriodCoupon(){
        $token = 'Bearer '.$_COOKIE['token'];
        $user_session_id = $_COOKIE['session_user_id'];

        $header = array(
            'Token' => $token,
            'Uses-Id' =>  $user_session_id
        );
        $result = $this->get('v1/utils/code-table',$header);
        $result = $result['responseText'];
        $coupon_period_code = array();
        foreach($result as $value){
            if($value['reference_column_name'] == 'default_setting_cd'){
                foreach($value['item_code'] as $key => $value){
                    if($value['item_code']=='CEP'){
                        $coupon_period_code = $value['item_setting_value'];

                    }
                }
            }
        }
        return $coupon_period_code;
    }
}
