<?php

namespace App\Http\Middleware;
use App\Http\Controllers\BaseController;
use Closure;
use Illuminate\Contracts\Auth\Guard;
class Admin extends BaseController
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
            if ($this->getProperty('role')['role_cd'] == 'SAM') {
                return $next($request);
             }else{
                return redirect()->guest('/');
            }
        return $next($request);

    }
}