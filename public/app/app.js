var tnjwebApp = {};
tnjwebApp = angular.module('tnjwebApp', ['ngRoute', 'ng-token-auth', 'xeditable']);
tnjwebApp.config(function($routeProvider, $locationProvider /*, $authProvider, USER_ROLES*/ ) {
    $routeProvider
        .when('/', {
            templateUrl: "view/users/login.html",
            controller: "LoginController"
        })
        .when('/password/reset', {
            templateUrl: "view/users/resetPassword.html",
            controller: "ResetPasswordController"
        })
        .when('/dashboard', {
            templateUrl: "view/dash/dashboard.html",
            controller: "DashController",
            reloadOnSearch: false
        })
        .when('/category', {
            templateUrl: "view/category/category.html",
            controller: "CategoryController",
            reloadOnSearch: false
        })
        .when('/slideshow', {
            templateUrl: "view/slide/slide.html",
            controller: "SlideController",
            reloadOnSearch: false
        })
        .when('/settings', {
            templateUrl: "view/users/editprofile.html",
            controller: "ProfileController"
        })
        .when('/user/new', {
            templateUrl: "view/users/new.html",
            controller: "AddNewUserController"
        })
        .otherwise({
            redirectTo: '/'
        });
    $locationProvider.html5Mode(true);
});
/*.config(['$httpProvider',
    function($httpProvider) {
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    }
]).config(function($authProvider) {
    $authProvider.configure({
        apiUrl: 'http://apinew.tealandjade.com',
        tokenValidationPath: '/api/admin/email/confirm/',
        signOutUrl: '/auth/sign_out',
        emailRegistrationPath: '/api/admin/register',
        accountUpdatePath: '/auth',
        accountDeletePath: '/auth',
        confirmationSuccessUrl: window.location.href,
        passwordResetPath: '/auth/password',
        passwordUpdatePath: '/auth/password',
        passwordResetSuccessUrl: window.location.href,
        emailSignInPath: '/api/admin/login',
        storage: 'cookies',
        forceValidateToken: false,
        validateOnPageLoad: true,
        proxyIf: function() {
            return false;
        },
        proxyUrl: '/proxy',
        omniauthWindowType: 'sameWindow',
    });
});*/
