$(document).ready(function(){
    clearAllText();
    $("#logo-picture").on('dragenter', function (e){
        e.preventDefault();

    });

    $("#logo-picture").on('dragover', function (e){
        e.preventDefault();
    });

    // $("#logo-picture").on('drop', function (e){
    //
    //     e.preventDefault();
    //     var image = e.originalEvent.dataTransfer.files;
    //     $("#modal-loading").modal('show');
    //     var div = 'img-logo';
    //     $("#save-temp").data('div',div);
    //     var formData = new FormData($('#frm-restaurant-profile')[0]);
    //     formData.append('logo',image[0]);
    //     $.ajax({
    //         url: baseUrl + '/add-logo-temp',
    //         type: "POST",
    //         dataType: "json",
    //         data: formData,
    //         contentType:false,
    //         cache: false,
    //         processData: false,
    //         success: function(data) {
    //
    //             if (data != "") {
    //                 $("#modalJcrop").modal({
    //                     keyboard: false
    //                 });
    //
    //                 $('#modalJcrop').on('shown.bs.modal', function(e) {
    //                     var selectWidth = 100;
    //                     var selectHeight = 100;
    //                     var minSize = [];
    //                     minSize[0] = selectWidth;
    //                     minSize[1] = selectHeight;
    //                     var max_box_width = $("#modalJcrop .my-thumbnail").width();
    //                     $('#view').attr('src', baseUrl + '/' + data.src);
    //                     $(".thumbnail .jcrop-holder img").attr('src', baseUrl + '/' + data.src);
    //                     var img = $('#view').val();
    //                     $('#view').replaceWith('<img id="view" src="' + baseUrl + '/' + data.src + '"/>');
    //                     // $('#imagePreviewLarge').removeAttr('style');
    //                     $('#path').val(data.src);
    //                     $('#type').val(1);
    //                     $('#title').val('merchant_logo');
    //                     $('#view').Jcrop({
    //                         aspectRatio: selectWidth / selectHeight,
    //                         boxWidth:  max_box_width, //Maximum width you want for your bigger images
    //                         setSelect:  [0, 0, selectWidth, selectHeight],
    //                         minSize:minSize,
    //                         maxSize:minSize,
    //                         onSelect: updateCoords,
    //                         onChange: updateCoords,
    //                         trueSize: [data.width,data.height],
    //                     }, function() {
    //                         $(".thumbnail .jcrop-holder .jcrop-keymgr").css("opacity", "0");
    //                     });
    //                     $(this).unbind('shown.bs.modal');
    //                 });
    //
    //
    //                 $('#modalJcrop').on('hide.bs.modal', function(e) {
    //                     $('#view').attr('src', '');
    //                     $('#view').css('width', '0px');
    //                     $('#view').css('height', '0px');
    //                     $('#view').data('Jcrop').destroy();
    //                     $(".thumbnail .jcrop-holder img").attr('src', '');
    //                     $(this).unbind('hide.bs.modal');
    //                 });
    //                 $("#modal-loading").modal('hide');
    //             }
    //         },
    //         error: function() {
    //             alert('Cannot Something wrong your image');
    //             $("#modal-loading").modal('hide');
    //             return false;
    //         },
    //
    //     });
    //
    // });
    $("#logo-picture").on('drop', function (e){

        e.preventDefault();
        var image = e.originalEvent.dataTransfer.files;
        $("#modal-loading").modal('show');
        var div = 'img-logo';
        $("#save-temp").data('div',div);
        var formData = new FormData($('#frm-restaurant-profile')[0]);
        formData.append('logo',image[0]);
        $.ajax({
            url: baseUrl + '/add-logo',
            type: "POST",
            dataType: "json",
            data: formData,
            contentType:false,
            cache: false,
            processData: false,
            success: function(data) {

                if(data.status == true){
                    $('#logo-picture').css('display', 'none');
                    $("#logo-table tr").append('<td><div class="img-upload" id="logo-empty">' +
                        '<img src="' + data.img_link + '" class="image-size" id="img-logo" />' +
                        '<input type="hidden" name="logo_id" id="logo_id" value="'+data.id+'">'+
                        '<input type="hidden" name="logo_title" id="logo_title" value="Merchant_logo">' +
                        '<input type="hidden" name="logo_filename" id="logo_filename" value="' + data.filename + '">' +
                        '<input type="hidden" name="logo_link" id="logo_link" value="' + data.img_link + '">' +
                        '<a class="icon-pop" onclick="removeLogo(this);">X</a>' +
                        '</div></td>');
                }else{
                    alert(data.message);
                }
                $("#modal-loading").modal('hide');
            },
            error: function() {
                alert('Cannot Something wrong your image');
                $("#modal-loading").modal('hide');
                return false;
            },

        });

    });

    $("#update-logo-picture").on('dragenter', function (e){
        e.preventDefault();

    });

    $("#update-logo-picture").on('dragover', function (e){
        e.preventDefault();
    });
    $("#update-logo-picture").on('drop', function (e){

        e.preventDefault();
        var image = e.originalEvent.dataTransfer.files;
        $("#modal-loading").modal('show');
        var div = 'img-logo';
        $("#save-temp").data('div',div);
        var formData = new FormData($('#frm-restaurant-profile')[0]);
        formData.append('logo',image[0]);
        $.ajax({
            url: baseUrl + '/add-logo',
            type: "POST",
            dataType: "json",
            data: formData,
            contentType:false,
            cache: false,
            processData: false,
            success: function(data) {

                if(data.status == true){
                    $('#update-logo-picture').css('display', 'none');
                    $("#logo-table tr").append('<td><div class="img-upload" id="logo-empty">' +
                        '<img src="' + data.img_link + '" class="image-size" id="img-logo" />' +
                        '<input type="hidden" name="logo_id" id="logo_id" value="'+data.id+'">'+
                        '<input type="hidden" name="logo_title" id="logo_title" value="Merchant_logo">' +
                        '<input type="hidden" name="logo_filename" id="logo_filename" value="' + data.filename + '">' +
                        '<input type="hidden" name="logo_link" id="logo_link" value="' + data.img_link + '">' +
                        '<a class="icon-pop" onclick="removeUpdateLogo(this);">X</a>' +
                        '</div></td>');
                }else{
                    alert(data.message);
                }
                $("#modal-loading").modal('hide');
            },
            error: function() {
                alert('Cannot Something wrong your image');
                $("#modal-loading").modal('hide');
                return false;
            },

        });

    });

    $("#banner-picture").on('dragenter', function (e){
        e.preventDefault();

    });

    $("#banner-picture").on('dragover', function (e){
        e.preventDefault();
    });

    // $("#banner-picture").on('drop', function (e){
    //     e.preventDefault();
    //     var image = e.originalEvent.dataTransfer.files;
    //     $("#modal-loading").modal('show');
    //     var div = 'img-banner';
    //     $("#save-temp").data('div',div);
    //     var formData = new FormData($('#frm-restaurant-profile')[0]);
    //     formData.append('banner',image[0]);
    //     $.ajax({
    //         url: baseUrl + '/add-banner-temp',
    //         type: "POST",
    //         dataType: "json",
    //         data: formData,
    //         contentType:false,
    //         cache: false,
    //         processData: false,
    //         success: function(data) {
    //
    //             if (data != "") {
    //                 $("#modalJcrop").modal({
    //                     keyboard: false
    //                 });
    //
    //                 $('#modalJcrop').on('shown.bs.modal', function(e) {
    //                     var selectWidth = 360;
    //                     var selectHeight = 190;
    //                     var minSize = [];
    //                     minSize[0] = selectWidth;
    //                     minSize[1] = selectHeight;
    //                     var max_box_width = $("#modalJcrop .my-thumbnail").width();
    //                     $('#view').attr('src', baseUrl + '/' + data.src);
    //                     $(".thumbnail .jcrop-holder img").attr('src', baseUrl + '/' + data.src);
    //                     var img = $('#view').val();
    //                     $('#view').replaceWith('<img id="view" src="' + baseUrl + '/' + data.src + '"/>');
    //                     // $('#imagePreviewLarge').removeAttr('style');
    //                     $('#path').val(data.src);
    //                     $('#type').val(1);
    //                     $('#title').val('merchant_banner');
    //                     $('#view').Jcrop({
    //                         aspectRatio: selectWidth / selectHeight,
    //                         boxWidth:  max_box_width, //Maximum width you want for your bigger images
    //                         setSelect:  [0, 0, selectWidth, selectHeight],
    //                         minSize:minSize,
    //                         maxSize:minSize,
    //                         onSelect: updateCoords,
    //                         onChange: updateCoords,
    //                         trueSize: [data.width,data.height],
    //                     }, function() {
    //                         $(".thumbnail .jcrop-holder .jcrop-keymgr").css("opacity", "0");
    //                     });
    //                     $(this).unbind('shown.bs.modal');
    //                 });
    //
    //
    //                 $('#modalJcrop').on('hide.bs.modal', function(e) {
    //                     $('#view').attr('src', '');
    //                     $('#view').css('width', '0px');
    //                     $('#view').css('height', '0px');
    //                     $('#view').data('Jcrop').destroy();
    //                     $(".thumbnail .jcrop-holder img").attr('src', '');
    //                     $(this).unbind('hide.bs.modal');
    //                 });
    //                 $("#modal-loading").modal('hide');
    //             }
    //         },
    //         error: function() {
    //             alert('Cannot Something wrong your image');
    //             $("#modal-loading").modal('hide');
    //             return false;
    //         },
    //
    //     });
    //
    // });
    $("#banner-picture").on('drop', function (e){
        e.preventDefault();
        var image = e.originalEvent.dataTransfer.files;
        $("#modal-loading").modal('show');
        var div = 'img-banner';
        $("#save-temp").data('div',div);
        var formData = new FormData($('#frm-restaurant-profile')[0]);
        formData.append('banner',image[0]);
        $.ajax({
            url: baseUrl + '/add-banner',
            type: "POST",
            dataType: "json",
            data: formData,
            contentType:false,
            cache: false,
            processData: false,
            success: function(data) {
                if(data.status == true){
                    $('#banner-picture').css('display','none');
                    $("#banner-table tr").append('<td><div class="banner-upload" id="banner-empty">'+
                        '<img src="'+data.img_link+'" class="banner-size" id="img-banner" />'+
                        '<input type="hidden" name="banner_id" id="banner_id" value="'+data.id+'">'+
                        '<input type="hidden" name="banner_title" id="banner_title" value="Merchant_banner">'+
                        '<input type="hidden" name="banner_filename" id="banner_filename" value="'+data.filename+'">'+
                        '<input type="hidden" name="banner_link" id="banner_link" value="'+data.img_link+'">'+
                        '<a class="icon-banner" onclick="removeBanner(this);">X</a>'+
                        '</div></td>');
                }else{
                    alert(data.message);
                }
                $("#modal-loading").modal('hide');

            },
            error: function() {
                alert('Cannot Something wrong your image');
                $("#modal-loading").modal('hide');
                return false;
            },

        });

    });

    $("#update-banner-picture").on('dragenter', function (e){
        e.preventDefault();

    });

    $("#update-banner-picture").on('dragover', function (e){
        e.preventDefault();
    });

    $("#update-banner-picture").on('drop', function (e){
        e.preventDefault();
        var image = e.originalEvent.dataTransfer.files;
        $("#modal-loading").modal('show');
        var div = 'img-banner';
        $("#save-temp").data('div',div);
        var formData = new FormData($('#frm-restaurant-profile')[0]);
        formData.append('banner',image[0]);
        $.ajax({
            url: baseUrl + '/add-banner',
            type: "POST",
            dataType: "json",
            data: formData,
            contentType:false,
            cache: false,
            processData: false,
            success: function(data) {
                if(data.status == true){
                    $('#update-banner-picture').css('display','none');
                    $("#banner-table tr").append('<td><div class="banner-upload" id="banner-empty">'+
                        '<img src="'+data.img_link+'" class="banner-size" id="img-banner" />'+
                        '<input type="hidden" name="banner_id" id="banner_id" value="'+data.id+'">'+
                        '<input type="hidden" name="banner_title" id="banner_title" value="Merchant_banner">'+
                        '<input type="hidden" name="banner_filename" id="banner_filename" value="'+data.filename+'">'+
                        '<input type="hidden" name="banner_link" id="banner_link" value="'+data.img_link+'">'+
                        '<a class="icon-banner" onclick="removeUpdateBanner(this);">X</a>'+
                        '</div></td>');
                }else{
                    alert(data.message);
                }
                $("#modal-loading").modal('hide');

            },
            error: function() {
                alert('Cannot Something wrong your image');
                $("#modal-loading").modal('hide');
                return false;
            },

        });

    });

    $("#photos-picture").on('dragenter', function (e){
        e.preventDefault();

    });

    $("#photos-picture").on('dragover', function (e){
        e.preventDefault();
    });
    $("#photos-picture").on('drop', function (e){
        e.preventDefault();
        var image = e.originalEvent.dataTransfer.files;
        $("#modal-loading").modal('show');
        var formData = new FormData($('#frm-restaurant-profile')[0]);
        for(var i = 0; i<image.length;i++){
            formData.append('photos_drag[]',image[i]);
        }

        $.ajax({
            url: baseUrl + '/add-photos-drag',
            type: "POST",
            dataType: "json",
            data: formData,
            contentType:false,
            processData: false,

            success: function(data) {
                console.log(data);
                var count = $('input[name="img_filename[]"]').length;
                if (data != "") {

                    for(var i = 0; i<data.length;i++){
                        if(count==0){
                            var title = 'Merchant_photos_'+(i+1);
                        }else{
                            var count = count+1;
                            var title = 'Merchant_photos_'+count;
                        }
                        $("#photos-upload tr").prepend('<td><div class="img-upload">' +
                            ' <img src="'+data[i].img_link+'" class="image-size" />' +
                            '  <a href="#" class="icon-pop" onclick="removeImage(this);">X</a>' +
                            '</div><input type="hidden" name="img_filename[]" value="'+data[i].filename+'">' +
                            '<input type="hidden" name="img_id[]" value="'+data[i].id+'">'+
                            '<input type="hidden" name="img_link[]" value="'+data[i].img_link+'"><input type="hidden" name="img_title[]" value="'+title+'"> </td>' );
                    }



                    $("#modal-loading").modal('hide');
                }
            },
            error: function() {
                alert('Cannot Something wrong your image');
                $("#modal-loading").modal('hide');
                return false;
            },

        });
    });

    $("#food-picture").on('dragenter', function (e){
        e.preventDefault();

    });

    $("#food-picture").on('dragover', function (e){
        e.preventDefault();
    });

    $("#food-picture").on('drop', function (e) {
        $("#modal-loading").modal('show');
        var image = e.originalEvent.dataTransfer.files;
        var formData = new FormData($('#frm-food-menu')[0]);
        formData.append('food-img',image[0])
        $.ajax({
            url: baseUrl + '/add-food-photo',
            type: "POST",
            dataType: "json",
            data: formData,
            contentType:false,
            cache: false,
            processData: false,

            success: function(data) {
                console.log(data);
                if (data != "") {

                    $('#img-food').attr('src',data.img_link);
                    $('#food_title').val('Merchant_food');
                    $('#food_filename').val(data.filename);
                    $('#food_link').val(data.img_link);

                    $("#modal-loading").modal('hide');
                }
            },
            error: function() {
                alert('Cannot Something wrong your image');
                $("#modal-loading").modal('hide');
                return false;
            },

        });
        e.preventDefault();

    });

});

$("#add-food-img").click(function(){
    $('#food-img').trigger('click');
});
$('#food-img').change(function(){
    $("#modal-loading").modal('show');
    var formData = new FormData($('#frm-food-menu')[0]);
    $.ajax({
        url: baseUrl + '/add-food-photo',
        type: "POST",
        dataType: "json",
        data: formData,
        contentType:false,
        cache: false,
        processData: false,

        success: function(data) {
            console.log(data);
            if (data != "") {

                $('#img-food').attr('src',data.img_link);
                $('#food_title').val('Merchant_food');
                $('#food_filename').val(data.filename);
                $('#food_link').val(data.img_link);

                $("#modal-loading").modal('hide');
            }
        },
        error: function() {
            alert('Cannot Something wrong your image');
            $("#modal-loading").modal('hide');
            return false;
        },

    });
});
$("#add-logo").click(function(){
    $('#logo').trigger('click');
});
//logo crop
// logo crop
//$('#logo').change(function(){
//    $("#modal-loading").modal('show');
//    var div = 'img-logo';
//    $("#save-temp").data('div',div);
//    var formData = new FormData($('#frm-restaurant-profile')[0]);
//
//    $.ajax({
//        url: baseUrl + '/add-logo-temp',
//        type: "POST",
//        dataType: "json",
//        data: formData,
//        contentType:false,
//        cache: false,
//        processData: false,
//
//        success: function(data) {
//
//            if (data != "") {
//                $("#modalJcrop").modal({
//                    keyboard: false
//                });
//
//                $('#modalJcrop').on('shown.bs.modal', function(e) {
//
//                    var selectWidth = 100;
//                    var selectHeight = 100;
//                    var minSize = [];
//                    minSize[0] = selectWidth;
//                    minSize[1] = selectHeight;
//                    var max_box_width = $("#modalJcrop .my-thumbnail").width();
//                    $('#view').attr('src', baseUrl + '/' + data.src);
//                    $(".thumbnail .jcrop-holder img").attr('src', baseUrl + '/' + data.src);
//                    var img = $('#view').val();
//                    $('#view').replaceWith('<img id="view" src="' + baseUrl + '/' + data.src + '"/>');
//                    // $('#imagePreviewLarge').removeAttr('style');
//
//                    $('#path').val(data.src);
//                    $('#type').val(1);
//                    $('#title').val('merchant_logo');
//                    $('#view').Jcrop({
//                        aspectRatio: selectWidth / selectHeight,
//                        boxWidth:  max_box_width, //Maximum width you want for your bigger images
//                        setSelect:  [0, 0, selectWidth, selectHeight],
//                        minSize:minSize,
//                        maxSize:minSize,
//                        onSelect: updateCoords,
//                        onChange: updateCoords,
//                        trueSize: [data.width,data.height],
//
//                    }, function() {
//                        $(".thumbnail .jcrop-holder .jcrop-keymgr").css("opacity", "0");
//                    });
//                    $(this).unbind('shown.bs.modal');
//                });
//
//
//                $('#modalJcrop').on('hide.bs.modal', function(e) {
//                    $('#view').attr('src', '');
//                    $('#view').css('width', '0px');
//                    $('#view').css('height', '0px');
//                    $('#view').data('Jcrop').destroy();
//                    $(".thumbnail .jcrop-holder img").attr('src', '');
//                    $(this).unbind('hide.bs.modal');
//                });
//                $("#modal-loading").modal('hide');
//            }
//        },
//        error: function() {
//            alert('Cannot Something wrong your image');
//            $("#modal-loading").modal('hide');
//            return false;
//        },
//
//    });
//});

// logo no crop
$('#logo').change(function(){
    $("#modal-loading").modal('show');

    var formData = new FormData($('#frm-restaurant-profile')[0]);

    $.ajax({
        url: baseUrl + '/add-logo',
        type: "POST",
        dataType: "json",
        data: formData,
        contentType:false,
        cache: false,
        processData: false,

        success: function(data) {

            if(data.status == true){
                $('#logo-picture').css('display', 'none');
                $("#logo-table tr").append('<td><div class="img-upload" id="logo-empty">' +
                    '<img src="' + data.img_link + '" class="image-size" id="img-logo" />' +
                    '<input type="hidden" name="logo_id" id="logo_id" value="'+data.id+'">'+
                    '<input type="hidden" name="logo_title" id="logo_title" value="Merchant_logo">' +
                    '<input type="hidden" name="logo_filename" id="logo_filename" value="' + data.filename + '">' +
                    '<input type="hidden" name="logo_link" id="logo_link" value="' + data.img_link + '">' +
                    '<a class="icon-pop" onclick="removeLogo(this);">X</a>' +
                    '</div></td>');
            }else{
                alert(data.message);
            }
            $("#modal-loading").modal('hide');
        },
        error: function() {
            alert('Cannot Something wrong your image');
            $("#modal-loading").modal('hide');
            return false;
        },

    });
});
$('#update-logo').change(function(){
    $("#modal-loading").modal('show');

    var formData = new FormData($('#frm-restaurant-profile')[0]);

    $.ajax({
        url: baseUrl + '/add-logo',
        type: "POST",
        dataType: "json",
        data: formData,
        contentType:false,
        cache: false,
        processData: false,

        success: function(data) {

            if(data.status == true){
                $('#update-logo-picture').css('display', 'none');
                $("#logo-table tr").append('<td><div class="img-upload" id="logo-empty">' +
                    '<img src="' + data.img_link + '" class="image-size" id="img-logo" />' +
                    '<input type="hidden" name="logo_id" id="logo_id" value="'+data.id+'">'+
                    '<input type="hidden" name="logo_title" id="logo_title" value="Merchant_logo">' +
                    '<input type="hidden" name="logo_filename" id="logo_filename" value="' + data.filename + '">' +
                    '<input type="hidden" name="logo_link" id="logo_link" value="' + data.img_link + '">' +
                    '<a class="icon-pop" onclick="removeUpdateLogo(this);">X</a>' +
                    '</div></td>');
            }else{
                alert(data.message);
            }
            $("#modal-loading").modal('hide');
        },
        error: function() {
            alert('Cannot Something wrong your image');
            $("#modal-loading").modal('hide');
            return false;
        },

    });
});

$("#add-banner").click(function(){
    $('#banner').trigger('click');
});

//$('#banner').change(function(){
//    $("#modal-loading").modal('show');
//    var div = 'img-banner';
//    $("#save-temp").data('div',div);
//    var formData = new FormData($('#frm-restaurant-profile')[0]);
//    console.log(formData);
//    $.ajax({
//        url: baseUrl + '/add-banner-temp',
//        type: "POST",
//        dataType: "json",
//        data: formData,
//        contentType:false,
//        cache: false,
//        processData: false,
//
//        success: function(data) {
//
//            if (data != "") {
//                $("#modalJcrop").modal({
//                    keyboard: false
//                });
//
//                $('#modalJcrop').on('shown.bs.modal', function(e) {
//                    var selectWidth = 360;
//                    var selectHeight = 190;
//                    var minSize = [];
//                    minSize[0] = selectWidth;
//                    minSize[1] = selectHeight;
//                    var max_box_width = $("#modalJcrop .my-thumbnail").width();
//                    $('#view').attr('src', baseUrl + '/' + data.src);
//                    $(".thumbnail .jcrop-holder img").attr('src', baseUrl + '/' + data.src);
//                    var img = $('#view').val();
//                    $('#view').replaceWith('<img id="view" src="' + baseUrl + '/' + data.src + '"/>');
//                    // $('#imagePreviewLarge').removeAttr('style');
//                    $('#path').val(data.src);
//                    $('#type').val(1);
//                    $('#title').val('merchant_banner');
//                    $('#view').Jcrop({
//                        aspectRatio: selectWidth / selectHeight,
//                        boxWidth:  max_box_width, //Maximum width you want for your bigger images
//                        setSelect:  [0, 0, selectWidth, selectHeight],
//                        minSize:minSize,
//                        maxSize:minSize,
//                        onSelect: updateCoords,
//                        onChange: updateCoords,
//                        trueSize: [data.width,data.height],
//                    }, function() {
//                        $(".thumbnail .jcrop-holder .jcrop-keymgr").css("opacity", "0");
//                    });
//                    $(this).unbind('shown.bs.modal');
//                });
//
//
//                $('#modalJcrop').on('hide.bs.modal', function(e) {
//                    $('#view').attr('src', '');
//                    $('#view').css('width', '0px');
//                    $('#view').css('height', '0px');
//                    $('#view').data('Jcrop').destroy();
//                    $(".thumbnail .jcrop-holder img").attr('src', '');
//                    $(this).unbind('hide.bs.modal');
//                });
//                $("#modal-loading").modal('hide');
//            }
//        },
//        error: function() {
//            alert('Cannot Something wrong your image');
//            $("#modal-loading").modal('hide');
//            return false;
//        },
//
//    });
//});
$('#banner').change(function(){
    $("#modal-loading").modal('show');

    var formData = new FormData($('#frm-restaurant-profile')[0]);

    $.ajax({
        url: baseUrl + '/add-banner',
        type: "POST",
        dataType: "json",
        data: formData,
        contentType:false,
        cache: false,
        processData: false,

        success: function(data) {

            if(data.status == true){
                $('#banner-picture').css('display','none');
                $("#banner-table tr").append('<td><div class="banner-upload" id="banner-empty">'+
                    '<img src="'+data.img_link+'" class="banner-size" id="img-banner" />'+
                    '<input type="hidden" name="banner_id" id="banner_id" value="'+data.id+'">'+
                    '<input type="hidden" name="banner_title" id="banner_title" value="Merchant_banner">'+
                    '<input type="hidden" name="banner_filename" id="banner_filename" value="'+data.filename+'">'+
                    '<input type="hidden" name="banner_link" id="banner_link" value="'+data.img_link+'">'+
                    '<a class="icon-banner" onclick="removeBanner(this);">X</a>'+
                    '</div></td>');
            }else{
                alert(data.message);
            }
            $("#modal-loading").modal('hide');
        },
        error: function() {
            alert('Cannot Something wrong your image');
            $("#modal-loading").modal('hide');
            return false;
        },

    });
});
$('#update-banner').change(function(){
    $("#modal-loading").modal('show');

    var formData = new FormData($('#frm-restaurant-profile')[0]);

    $.ajax({
        url: baseUrl + '/add-banner',
        type: "POST",
        dataType: "json",
        data: formData,
        contentType:false,
        cache: false,
        processData: false,

        success: function(data) {

            if(data.status == true){
                $('#update-banner-picture').css('display','none');
                $("#banner-table tr").append('<td><div class="banner-upload" id="banner-empty">'+
                    '<img src="'+data.img_link+'" class="banner-size" id="img-banner" />'+
                    '<input type="hidden" name="banner_id" id="banner_id" value="'+data.id+'">'+
                    '<input type="hidden" name="banner_title" id="banner_title" value="Merchant_banner">'+
                    '<input type="hidden" name="banner_filename" id="banner_filename" value="'+data.filename+'">'+
                    '<input type="hidden" name="banner_link" id="banner_link" value="'+data.img_link+'">'+
                    '<a class="icon-banner" onclick="removeUpdateBanner(this);">X</a>'+
                    '</div></td>');
            }else{
                alert(data.message);
            }
            $("#modal-loading").modal('hide');
        },
        error: function() {
            alert('Cannot Something wrong your image');
            $("#modal-loading").modal('hide');
            return false;
        },

    });
});
$('#promo-img').change(function(){

    $("#modal-loading").modal('show');

    var formData = new FormData($('#frm-restaurant-promotion')[0]);

    $.ajax({
        url: baseUrl + '/add-promotion-img',
        type: "POST",
        dataType: "json",
        data: formData,
        contentType:false,
        cache: false,
        processData: false,

        success: function(data) {

            if(data.status == true){
                $('#banner-promotion').css('display','none');
                $("#promotion-table tr").append('<td><div class="img-upload-promo">'+
                    '<img src="'+data.img_link+'" class="image-promo-size" id="img-banner" />'+
                    '<input type="hidden" name="promo_id" id="promo_id" value="'+data.id+'">'+
                    '<input type="hidden" name="promo_title" id="promo_title" value="Banner Promotion">'+
                    '<input type="hidden" name="promo_filename" id="promo_filename" value="'+data.filename+'">'+
                    '<input type="hidden" name="promo_link" id="promo_link" value="'+data.img_link+'">'+
                    '<a class="icon-banner-promo" onclick="removePromotion(this);">X</a>'+
                    '</div></td>');
            }else{
                alert(data.message);
            }
            $("#modal-loading").modal('hide');
        },
        error: function() {
            alert('Cannot Something wrong your image');
            $("#modal-loading").modal('hide');
            return false;
        },

    });
});
$('#menu-img').change(function(){

    $("#modal-loading").modal('show');

    var formData = new FormData($('#frm-food-item')[0]);

    $.ajax({
        url: baseUrl + '/add-menu-img',
        type: "POST",
        dataType: "json",
        data: formData,
        contentType:false,
        cache: false,
        processData: false,

        success: function(data) {
            console.log(data);
            if(data.status == true){
                $('#menu-promotion').css('display','none');
                $('#link').val(data.img_link);
                $("#menu-table tr").append('<td><div class="img-upload-promo">'+
                    '<img src="'+data.img_link+'" class="image-promo-size" id="img-banner" />'+
                    '<input type="hidden" name="menu_id" id="menu_id" value="'+data.id+'">'+
                    '<input type="hidden" name="menu_title" id="menu_title" value="Banner Menu">'+
                    '<input type="hidden" name="menu_filename" id="menu_filename" value="'+data.filename+'">'+
                    '<input type="hidden" name="menu_link" id="menu_link" value="'+data.img_link+'">'+
                    '<a class="icon-banner-promo" onclick="removeMenu(this);">X</a>'+
                    '</div></td>');
            }else{
                alert(data.message);
            }
            $("#modal-loading").modal('hide');
        },
        error: function(data) {
            console.log(data);
            alert('Cannot Something wrong your image');
            $("#modal-loading").modal('hide');
            return false;
        },

    });
});
$("#add-photos").click(function(){
    $('#photos').trigger('click');
});
$('#photos').change(function(){
    $("#modal-loading").modal('show');
    var formData = new FormData($('#frm-restaurant-profile')[0]);
    $.ajax({
        url: baseUrl + '/add-photos',
        type: "POST",
        dataType: "json",
        data: formData,
        contentType:false,
        cache: false,
        processData: false,

        success: function(data) {
            var count = $('input[name="img_filename[]"]').length;
            if (data != "") {

                for(var i = 0; i<data.length;i++){
                    if(count==0){
                        var title = 'Merchant_photos_'+(i+1);
                    }else{
                        var count = count+1;
                        var title = 'Merchant_photos_'+count;
                    }
                    $("#photos-upload tr").prepend('<td><div class="img-upload">' +
                        ' <img src="'+data[i].img_link+'" class="image-size" />' +
                        '  <a class="icon-pop" onclick="removeImage(this);">X</a>' +
                        '</div><input type="hidden" name="img_filename[]" value="'+data[i].filename+'">' +
                        '<input type="hidden" name="img_id[]" value="'+data[i].id+'">'+
                        '<input type="hidden" name="img_link[]" value="'+data[i].img_link+'"><input type="hidden" name="img_title[]" value="'+title+'"> </td>' );
                }





                $("#modal-loading").modal('hide');
            }
        },
        error: function() {
            alert('Cannot Something wrong your image');
            $("#modal-loading").modal('hide');
            return false;
        },

    });
});

function updateCoords(c) {
    $('#x').val(c.x.toFixed(0));
    $('#y').val(c.y.toFixed(0));
    $('#w').val(c.w.toFixed(0));
    $('#h').val(c.h.toFixed(0));
    $(".thumbnail .jcrop-holder .jcrop-keymgr").css("opacity", "0");
};

$("#save-temp").click(function() {
    $("#modal-loading").modal('show');
    var div = $("#save-temp").data('div');
    var formData = new FormData($('#frmJcrop')[0]);

    $.ajax({
        url: baseUrl + '/add-image-crop',
        processData: false,
        contentType: false,
        type: "POST",
        dataType: "json",
        data: formData,

        success: function (data) {
            console.log(data);
            if (data != "") {
                if(div=='img-logo'){
                    $('#logo-picture').css('display','none');
                    $("#logo-table tr").append('<td><div class="img-upload" id="logo-empty">'+
                        '<img src="'+data.img_link+'" class="image-size" id="img-logo" />'+
                        '<input type="hidden" name="logo_title" id="logo_title" value="Merchant_logo">'+
                        '<input type="hidden" name="logo_filename" id="logo_filename" value="'+data.filename+'">'+
                        '<input type="hidden" name="logo_link" id="logo_link" value="'+data.img_link+'">'+
                        '<a class="icon-pop" onclick="removeLogo(this);">X</a>'+
                        '</div></td>');
                }else if(div == 'img-banner'){
                    $('#banner-picture').css('display','none');
                    $("#banner-table tr").append('<td><div class="banner-upload" id="banner-empty">'+
                        '<img src="'+data.img_link+'" class="banner-size" id="img-banner" />'+
                        '<input type="hidden" name="banner_title" id="banner_title" value="Merchant_banner">'+
                        '<input type="hidden" name="banner_filename" id="banner_filename" value="'+data.filename+'">'+
                        '<input type="hidden" name="banner_link" id="banner_link" value="'+data.img_link+'">'+
                        '<a class="icon-banner" onclick="removeBanner(this);">X</a>'+
                        '</div></td>');
                }

                $('#modalJcrop').modal('hide');
                $("#modal-loading").modal('hide');
            }
        }
    });
});
function clearAllText() {
    $('#logo_title').val('');
    $('#logo_filename').val('');
    $('#logo_link').val('');
    $('#banner_title').val('');
    $('#banner_filename').val('');
    $('#banner_link').val('');
    $('#food_title').val('');
    $('#food_filename').val('');
    $('#food_link').val('');
}