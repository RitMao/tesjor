
function defaultImage() {

    var latitude = 11.5500;
    var longitude = 104.9167;


    var zoom = 15;

    var LatLng = new google.maps.LatLng(latitude, longitude);
    document.getElementById('lat').value = latitude;
    document.getElementById('lng').value = longitude;
    var mapProp = {
        center: LatLng,
        zoom:12,
        mapTypeId:google.maps.MapTypeId.ROADMAP
    };
    var map=new google.maps.Map(document.getElementById("map-canvas"), mapProp);
    var marker = new google.maps.Marker({
        position: LatLng,
        map: map,
        title: 'Maker',
        draggable: true
    });


    google.maps.event.addListener(marker, 'dragend', function(event) {

        document.getElementById('lat').value = event.latLng.lat();
        document.getElementById('lng').value = event.latLng.lng();



    });
    google.maps.event.addListener(marker, 'places_changed', function(event) {

        document.getElementById('lat').value = event.latLng.lat();
        document.getElementById('lng').value = event.latLng.lng();



    });
}
google.maps.event.addDomListener(window, 'load', defaultImage);

